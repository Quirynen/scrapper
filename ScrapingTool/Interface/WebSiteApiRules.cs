﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScrapingTool.Interface
{
    interface WebSiteApiRules
    {
        void CallAPIAsync();
        void BuildMatchs();
        void InsertMatch();
        Task<string> getResponse();
    }
}
