﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Interface
{
    interface ScrappingWebsiteRules
    {
        HtmlNodeCollection scrapOdds(string xpath, HtmlNode node);

        HtmlNodeCollection scrapTeams(string xpath, HtmlNode node);

        HtmlNodeCollection scrapHoursGame(string xpath, HtmlNode node);

        void BuildMatchs();

        void InsertMatch();


    }
}
