﻿using HtmlAgilityPack;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using ScrapingTool.Scrapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ScrapingTool.PremierLeague
{
    public class Ladbrokes : ScrappingWebsiteRules
    {
        ScrappingWebSite _scrapper;

        List<MatchFoot> listMatchFoot;
        //Scapper bwin
        public Ladbrokes()
        {
            _scrapper = new ScrappingWebSite("https://sports.ladbrokes.be/en/t/40074/Premier-League", new string[] { "//*[contains(concat(' ', normalize-space(@class), ' '), ' seln-name ')]" }, null, -1);
            BuildMatchs();
            this._scrapper.driver.Close();
        }
        public void BuildMatchs()
        {
            HtmlNodeCollection matchsGrid = this._scrapper.retrieveData("//*[contains(@class, 'mkt mkt_content')]");
            listMatchFoot = new List<MatchFoot>();
            for (int i = 0; i <= matchsGrid.Count - 1; i++)
            {
                //On récupère les cotes selon le format : 1 1 1 1 1 /// Seulement les trois premières sont à utiliser
                try
                {
                    HtmlNodeCollection time = this.scrapHoursGame(".//span[contains(@class, 'time')]", matchsGrid[i]);
                    HtmlNodeCollection participants = this.scrapTeams(".//span[contains(concat(' ', normalize-space(@class), ' '), ' seln-name ')]", matchsGrid[i]);
                    HtmlNodeCollection odds = this.scrapOdds(".//*[contains(@class, 'price dec')]", matchsGrid[i]);
                    if (time != null && participants != null && odds != null)
                    {
                        MatchFoot match = new MatchFoot(transformTextToDate(time[0].InnerText), participants[0].InnerText, participants[1].InnerText, double.Parse(odds[0].InnerText, CultureInfo.InvariantCulture), double.Parse(odds[1].InnerText, CultureInfo.InvariantCulture), double.Parse(odds[2].InnerText, CultureInfo.InvariantCulture));
                        listMatchFoot.Add(match);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception" + ex + "Index est " + i);
                }

            }
            this.InsertMatch();
        }

        private DateTime transformTextToDate(string innerText)
        {
            try
            {
                string[] stDates = innerText.Split(':');
                return new DateTime().AddDays(1).AddHours(double.Parse(stDates[0])).AddMinutes(double.Parse(stDates[1]));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème dans la création d'une date " + ex);
                throw new Exception();
            }
        }

        public void InsertMatch()
        {
            try
            {
                MatchFootService.insertMatchFoot(listMatchFoot, "Ladbrokes");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert Ladbrokes Match", ex);
                throw new Exception();
            }
        }

        public HtmlNodeCollection scrapHoursGame(string xpath, HtmlNode node)
        {
            try
            {
                return node.SelectNodes(xpath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème scrapHoursGame Ladbrokes Match", ex);
                throw new Exception();
            }
        }

        public HtmlNodeCollection scrapOdds(string xpath, HtmlNode node)
        {
            try
            {
                return node.SelectNodes(xpath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème scrapOdds Ladbrokes Match", ex);
                throw new Exception();
            }
        }

        public HtmlNodeCollection scrapTeams(string xpath, HtmlNode node)
        {
            try
            {
                return node.SelectNodes(xpath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème scrapTeams Ladbrokes Match", ex);
                throw new Exception();
            }
        }
    }
}
