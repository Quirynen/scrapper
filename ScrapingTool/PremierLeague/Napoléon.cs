﻿using Newtonsoft.Json;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static ScrapingTool.Model.NapoleonResponse;

namespace ScrapingTool.PremierLeague
{
    public class Napoleon : WebSiteApiRules
    {
        HttpClient _client;
        string response;
        NapoleonResp apiReponse;
        List<MatchFoot> listMatch;
        public Napoleon()
        {
            listMatch = new List<MatchFoot>();
            _client = Driver.Client.Instance;
            CallAPIAsync();
        }
        public void BuildMatchs()
        {
            foreach (var _event in apiReponse.events)
            {
                if (_event.betOffers.Count > 0 && _event.betOffers[0].outcomes.Count >= 3 && _event.betOffers[0].outcomes[0].oddsFractional != null && _event.betOffers[0].outcomes[0].oddsFractional != "Evens" && _event.betOffers[0].outcomes[1].oddsFractional != null && _event.betOffers[0].outcomes[1].oddsFractional != "Evens" && _event.betOffers[0].outcomes[2].oddsFractional != null && _event.betOffers[0].outcomes[2].oddsFractional != "Evens")
                {
                    double oddA = fractionToDouble(_event.betOffers[0].outcomes[0].oddsFractional);
                    double oddB = fractionToDouble(_event.betOffers[0].outcomes[2].oddsFractional);
                    double oddX = fractionToDouble(_event.betOffers[0].outcomes[1].oddsFractional);
                    MatchFoot match = new MatchFoot(_event.@event.start.ToLocalTime(), _event.@event.homeName, _event.@event.awayName, oddA, oddX, oddB);
                    listMatch.Add(match);
                    Console.WriteLine(match);
                }

            }
            this.InsertMatch();
        }

        public async void CallAPIAsync()
        {
            response = await getResponse();
            apiReponse = JsonConvert.DeserializeObject<NapoleonResp>(response);
            BuildMatchs();
        }
        private double fractionToDouble(string fraction)
        {
            fraction.Trim();
            string[] numbers = fraction.Split('/');
            return (double.Parse(numbers[0]) / double.Parse(numbers[1])) + 1;
        }
        public void InsertMatch()
        {
            try
            {
                MatchFootService.insertMatchFoot(listMatch, "Napoleon");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert Napoleon Match", ex);
                throw new Exception();
            }
        }
        public async Task<string> getResponse()
        {
            return await _client.GetAsync("https://eu-offering.kambicdn.org/offering/v2018/ngbe/listView/football/england/premier_league.json?lang=nl_BE&market=BE&client_id=2&channel_id=1&ncid=1575210791557&useCombined=true").Result.Content.ReadAsStringAsync();

        }
    }
}
