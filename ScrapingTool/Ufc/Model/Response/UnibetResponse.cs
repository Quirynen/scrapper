﻿namespace ScrapingTool.Ufc
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class UnibetResponse
    {
        [JsonProperty("events")]
        public EventElement[] Events { get; set; }

        [JsonProperty("terms")]
        public Term[] Terms { get; set; }

        [JsonProperty("activeTermIds")]
        public string[] ActiveTermIds { get; set; }

        [JsonProperty("soonMode")]
        public string SoonMode { get; set; }

        [JsonProperty("categoryGroups")]
        public object[] CategoryGroups { get; set; }

        [JsonProperty("activeCategories")]
        public object[] ActiveCategories { get; set; }

        [JsonProperty("activeEventTypes")]
        public string[] ActiveEventTypes { get; set; }

        [JsonProperty("eventTypes")]
        public string[] EventTypes { get; set; }

        [JsonProperty("defaultEventType")]
        public string DefaultEventType { get; set; }
    }

    public partial class EventElement
    {
        [JsonProperty("event")]
        public EventEvent Event { get; set; }

        [JsonProperty("betOffers")]
        public BetOffer[] BetOffers { get; set; }
    }

    public partial class BetOffer
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("closed")]
        public DateTimeOffset Closed { get; set; }

        [JsonProperty("criterion")]
        public Criterion Criterion { get; set; }

        [JsonProperty("betOfferType")]
        public BetOfferType BetOfferType { get; set; }

        [JsonProperty("eventId")]
        public long EventId { get; set; }

        [JsonProperty("outcomes")]
        public Outcome[] Outcomes { get; set; }

        [JsonProperty("tags")]
        public string[] Tags { get; set; }

        [JsonProperty("cashOutStatus")]
        public string CashOutStatus { get; set; }

        [JsonProperty("extra", NullValueHandling = NullValueHandling.Ignore)]
        public string Extra { get; set; }
    }

    public partial class BetOfferType
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("englishName")]
        public string EnglishName { get; set; }

        [JsonProperty("termKey", NullValueHandling = NullValueHandling.Ignore)]
        public string? TermKey { get; set; }
    }

    public partial class Criterion
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("englishLabel")]
        public string EnglishLabel { get; set; }

        [JsonProperty("order")]
        public long[] Order { get; set; }
    }

    public partial class Outcome
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("label")]
        public long Label { get; set; }

        [JsonProperty("englishLabel")]
        public long EnglishLabel { get; set; }

        [JsonProperty("odds")]
        public long Odds { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("betOfferId")]
        public long BetOfferId { get; set; }

        [JsonProperty("changedDate")]
        public DateTimeOffset ChangedDate { get; set; }

        [JsonProperty("oddsFractional")]
        public string OddsFractional { get; set; }

        [JsonProperty("oddsAmerican")]
        public long OddsAmerican { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("cashOutStatus")]
        public string CashOutStatus { get; set; }
    }

    public partial class EventEvent
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("nameDelimiter")]
        public string NameDelimiter { get; set; }

        [JsonProperty("englishName")]
        public string EnglishName { get; set; }

        [JsonProperty("homeName")]
        public string HomeName { get; set; }

        [JsonProperty("awayName")]
        public string AwayName { get; set; }

        [JsonProperty("start")]
        public DateTimeOffset Start { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("groupId")]
        public long GroupId { get; set; }

        [JsonProperty("path")]
        public BetOfferType[] Path { get; set; }

        [JsonProperty("nonLiveBoCount")]
        public long NonLiveBoCount { get; set; }

        [JsonProperty("sport")]
        public string Sport { get; set; }

        [JsonProperty("tags")]
        public string[] Tags { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }

    public partial class Term
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("termKey")]
        public string TermKey { get; set; }

        [JsonProperty("localizedName")]
        public string LocalizedName { get; set; }

        [JsonProperty("parentId")]
        public string ParentId { get; set; }

        [JsonProperty("englishName")]
        public string EnglishName { get; set; }
    }


}
