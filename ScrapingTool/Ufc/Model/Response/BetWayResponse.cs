﻿namespace ScrapingTool.Ufc
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class BetWayResponse
    {
        [JsonProperty("Events")]
        public Event[] Events { get; set; }

        [JsonProperty("Markets")]
        public Market[] Markets { get; set; }

        [JsonProperty("Outcomes")]
        public Outcome[] Outcomes { get; set; }

        [JsonProperty("Scoreboards")]
        public Scoreboard[] Scoreboards { get; set; }

        [JsonProperty("MethodName")]
        public string MethodName { get; set; }

        [JsonProperty("MethodResult")]
        public string MethodResult { get; set; }

        [JsonProperty("Success")]
        public bool Success { get; set; }

        [JsonProperty("CorrelationId")]
        public Guid CorrelationId { get; set; }
    }

    public partial class Event
    {
        [JsonProperty("Version")]
        public long Version { get; set; }

        [JsonProperty("Id")]
        public long IdBetWay { get; set; }

        [JsonProperty("GroupName")]
        public string GroupName { get; set; }

        [JsonProperty("SubCategoryName")]
        public string SubCategoryName { get; set; }

        [JsonProperty("CategoryCName")]
        public string CategoryCName { get; set; }

        [JsonProperty("CategoryName")]
        public string CategoryName { get; set; }

        [JsonProperty("HomeTeamName")]
        public string HomeTeamName { get; set; }

        [JsonProperty("AwayTeamName")]
        public string AwayTeamName { get; set; }

        [JsonProperty("EventName")]
        public string EventNameBetWay { get; set; }

        [JsonProperty("Milliseconds")]
        public long Milliseconds { get; set; }

        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("Time")]
        public string Time { get; set; }

        [JsonProperty("Markets")]
        public long[] Markets { get; set; }

        [JsonProperty("CouponMarketId")]
        public long CouponMarketId { get; set; }

        [JsonProperty("FilterMarkets")]
        public FilterMarkets FilterMarkets { get; set; }

        [JsonProperty("IsSuspended")]
        public bool IsSuspendedBetWay { get; set; }

        [JsonProperty("LiveStreamingMobileStatus")]
        public object LiveStreamingMobileStatus { get; set; }

        [JsonProperty("LiveStreamingProviderType")]
        public object LiveStreamingProviderType { get; set; }

        [JsonProperty("LiveStreamingDesktopStatus")]
        public object LiveStreamingDesktopStatus { get; set; }

        [JsonProperty("IsOutright")]
        public bool IsOutright { get; set; }

        [JsonProperty("FeedId")]
        public object FeedId { get; set; }

        [JsonProperty("SuperTournamentCName")]
        public object SuperTournamentCName { get; set; }

        [JsonProperty("SuperTournamentName")]
        public object SuperTournamentName { get; set; }

        [JsonProperty("MarketGroups")]
        public MarketGroups MarketGroupsBetWay { get; set; }

        [JsonProperty("IsLive")]
        public bool IsLiveBetway { get; set; }

        [JsonProperty("IsPremium")]
        public bool IsPremium { get; set; }

        [JsonProperty("HomeTeamCName")]
        public string HomeTeamCName { get; set; }

        [JsonProperty("AwayTeamCName")]
        public string AwayTeamCName { get; set; }

        [JsonProperty("GroupCName")]
        public string GroupCName { get; set; }

        [JsonProperty("SubCategoryCName")]
        public string SubCategoryCName { get; set; }

        [JsonProperty("VenueTime")]
        public object VenueTime { get; set; }

        [JsonProperty("Venue")]
        public string Venue { get; set; }

        [JsonProperty("Title")]
        public object Title { get; set; }

        [JsonProperty("IsLiveTraded")]
        public bool IsLiveTraded { get; set; }

        [JsonProperty("AmericanFormat")]
        public bool AmericanFormat { get; set; }

        [JsonProperty("NeutralVenue")]
        public bool NeutralVenue { get; set; }

        [JsonProperty("SurfaceCName")]
        public string? SurfaceCName { get; set; }

        [JsonProperty("SurfaceName")]
        public string SurfaceName { get; set; }

        [JsonProperty("SportVariationCName")]
        public string? SportVariationCName { get; set; }

        [JsonProperty("SportVariationName")]
        public string SportVariationName { get; set; }

        [JsonProperty("SportsRadarId")]
        public object SportsRadarId { get; set; }

        [JsonProperty("IsVirtual")]
        public bool IsVirtual { get; set; }

        [JsonProperty("IsBetBuilderSupported")]
        public bool IsBetBuilderSupported { get; set; }
    }

    public partial class FilterMarkets
    {
        [JsonProperty("fight-winner")]
        public long FightWinner { get; set; }
    }

    public partial class MarketGroups
    {
        [JsonProperty("main-markets")]
        public MainMarkets MainMarkets { get; set; }
    }

    public partial class MainMarkets
    {
        [JsonProperty("GroupName")]
        public string GroupName { get; set; }

        [JsonProperty("MarketIds")]
        public long[] MarketIds { get; set; }
    }

    public partial class Market
    {
        [JsonProperty("Version")]
        public long Version { get; set; }

        [JsonProperty("Id")]
        public long IdBetWay { get; set; }

        [JsonProperty("Title")]
        public string TitleBetWay { get; set; }

        [JsonProperty("IsSuspended")]
        public bool IsSuspendedBetWay { get; set; }

        [JsonProperty("Handicap")]
        public long Handicap { get; set; }

        [JsonProperty("Headers")]
        public string[] Headers { get; set; }

        [JsonProperty("Outcomes")]
        public long[][] Outcomes { get; set; }

        [JsonProperty("EventId")]
        public long EventIdBetWay { get; set; }

        [JsonProperty("EachWayActive")]
        public bool EachWayActive { get; set; }

        [JsonProperty("EachWayFractionDen")]
        public long EachWayFractionDen { get; set; }

        [JsonProperty("EachWayPosition")]
        public long EachWayPosition { get; set; }

        [JsonProperty("OutcomeGroups")]
        public OutcomeGroups OutcomeGroups { get; set; }

        [JsonProperty("TypeCName")]
        public string TypeCName { get; set; }

        [JsonProperty("TemplateId")]
        public long TemplateId { get; set; }

        [JsonProperty("MarketGroupCName")]
        public string MarketGroupCName { get; set; }

        [JsonProperty("MarketGroupName")]
        public string MarketGroupName { get; set; }

        [JsonProperty("CashOutActive")]
        public bool CashOutActive { get; set; }

        [JsonProperty("FixedPriceAvailable")]
        public bool FixedPriceAvailable { get; set; }

        [JsonProperty("StartingPriceAvailable")]
        public bool StartingPriceAvailable { get; set; }

        [JsonProperty("AmericanFormat")]
        public bool AmericanFormat { get; set; }

        [JsonProperty("CouponHeaders")]
        public string[] CouponHeaders { get; set; }

        [JsonProperty("SituationIndex")]
        public object SituationIndex { get; set; }

        [JsonProperty("IsBetBuilderSupported")]
        public bool IsBetBuilderSupported { get; set; }
    }

    public partial class OutcomeGroups
    {
        [JsonProperty("home")]
        public Away Home { get; set; }

        [JsonProperty("away")]
        public Away Away { get; set; }
    }

    public partial class Away
    {
        [JsonProperty("outcomes")]
        public long[] Outcomes { get; set; }
    }

    public partial class Outcome
    {
        [JsonProperty("Version")]
        public long Version { get; set; }

        [JsonProperty("Id")]
        public long IdBetWay { get; set; }

        [JsonProperty("IsDisplay")]
        public bool IsDisplay { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("OddsDecimal")]
        public double OddsDecimal { get; set; }

        [JsonProperty("OddsDecimalDisplay")]
        public string OddsDecimalDisplay { get; set; }

        [JsonProperty("OddsNum")]
        public long OddsNum { get; set; }

        [JsonProperty("OddsDen")]
        public long OddsDen { get; set; }

        [JsonProperty("EventId")]
        public long EventId { get; set; }

        [JsonProperty("MarketId")]
        public long MarketId { get; set; }

        [JsonProperty("ColumnId")]
        public long ColumnId { get; set; }

        [JsonProperty("RowId")]
        public long RowId { get; set; }

        [JsonProperty("GroupCName")]
        public object GroupCName { get; set; }

        [JsonProperty("BetName")]
        public string BetName { get; set; }

        [JsonProperty("CouponName")]
        public string CouponName { get; set; }

        [JsonProperty("HandicapDisplay")]
        public string HandicapDisplay { get; set; }

        [JsonProperty("Handicap")]
        public object Handicap { get; set; }

        [JsonProperty("SortIndex")]
        public long SortIndex { get; set; }
    }

    public partial class Scoreboard
    {
        [JsonProperty("EventId")]
        public long EventId { get; set; }

        [JsonProperty("CategoryCName")]
        public string CategoryCName { get; set; }

        [JsonProperty("State")]
        public State State { get; set; }

        [JsonProperty("IsLive")]
        public bool IsLive { get; set; }

        [JsonProperty("Statistics")]
        public Statistics Statistics { get; set; }

        [JsonProperty("Incidents")]
        public object[] Incidents { get; set; }

        [JsonProperty("Periods")]
        public Period[] Periods { get; set; }
    }

    public partial class Period
    {
        [JsonProperty("PeriodIndex")]
        public long PeriodIndex { get; set; }

        [JsonProperty("PeriodCName")]
        public string PeriodCName { get; set; }

        [JsonProperty("PeriodName")]
        public string PeriodName { get; set; }
    }

    public partial class State
    {
        [JsonProperty("EventId")]
        public long EventId { get; set; }

        [JsonProperty("CurrentPeriodIndex")]
        public long CurrentPeriodIndex { get; set; }

        [JsonProperty("ActiveTime")]
        public string ActiveTime { get; set; }

        [JsonProperty("EventComment")]
        public object EventComment { get; set; }

        [JsonProperty("LastDynamicComment")]
        public object LastDynamicComment { get; set; }

        [JsonProperty("PeriodComment")]
        public string PeriodComment { get; set; }

        [JsonProperty("Time")]
        public long Time { get; set; }

        [JsonProperty("TimerType")]
        public long TimerType { get; set; }

        [JsonProperty("TimerRunning")]
        public bool TimerRunning { get; set; }

        [JsonProperty("TimerDisplayed")]
        public bool TimerDisplayed { get; set; }

        [JsonProperty("TotalPeriods")]
        public long TotalPeriods { get; set; }
    }

    public partial class Statistics
    {
        [JsonProperty("Score")]
        public Score Score { get; set; }
    }

    public partial class Score
    {
        [JsonProperty("Values")]
        public long[] Values { get; set; }
    }


}
