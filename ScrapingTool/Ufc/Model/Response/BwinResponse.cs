﻿namespace ScrapingTool.Ufc
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class BwinResponse
    {
        [JsonProperty("highlights")]
        public Highlight[] Highlights { get; set; }
    }

    public partial class Highlight
    {
        [JsonProperty("games")]
        public Game[] Games { get; set; }

        [JsonProperty("participants")]
        public ParticipantBwin[] Participants { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public SourceNameClass Name { get; set; }

        [JsonProperty("sourceId")]
        public long SourceId { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("fixtureType")]
        public string FixtureType { get; set; }

        [JsonProperty("context")]
        public string Context { get; set; }

        [JsonProperty("addons")]
        public Addons Addons { get; set; }

        [JsonProperty("stage")]
        public string Stage { get; set; }

        [JsonProperty("liveType")]
        public string LiveType { get; set; }

        [JsonProperty("liveAlert")]
        public bool LiveAlert { get; set; }

        [JsonProperty("startDate")]
        public DateTimeOffset StartDate { get; set; }

        [JsonProperty("cutOffDate")]
        public DateTimeOffset CutOffDate { get; set; }

        [JsonProperty("sport")]
        public Sport Sport { get; set; }

        [JsonProperty("competition")]
        public Ion Competition { get; set; }

        [JsonProperty("region")]
        public Ion Region { get; set; }

        [JsonProperty("viewType")]
        public string ViewType { get; set; }

        [JsonProperty("isOpenForBetting")]
        public bool IsOpenForBetting { get; set; }

        [JsonProperty("isVirtual")]
        public bool IsVirtual { get; set; }
    }

    public partial class Addons
    {
    }

    public partial class Ion
    {
        [JsonProperty("statistics", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Statistics { get; set; }

        [JsonProperty("sportId")]
        public long SportId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("parentId")]
        public long ParentId { get; set; }

        [JsonProperty("name")]
        public SourceNameClass Name { get; set; }

        [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }
    }

    public partial class SourceNameClass
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("sign")]
        public string Sign { get; set; }
    }

    public partial class Game
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public SourceNameClass Name { get; set; }

        [JsonProperty("results")]
        public Result[] Results { get; set; }

        [JsonProperty("templateId")]
        public long TemplateId { get; set; }

        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }

        [JsonProperty("resultOrder")]
        public string ResultOrder { get; set; }

        [JsonProperty("combo1")]
        public string Combo1 { get; set; }

        [JsonProperty("combo2")]
        public string Combo2 { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("isMain")]
        public bool IsMain { get; set; }

        [JsonProperty("grouping")]
        public Grouping Grouping { get; set; }
    }

    public partial class Grouping
    {
        [JsonProperty("grid", NullValueHandling = NullValueHandling.Ignore)]
        public string Grid { get; set; }

        [JsonProperty("detailed")]
        public Detailed[] Detailed { get; set; }
    }

    public partial class Detailed
    {
        [JsonProperty("group")]
        public long Group { get; set; }

        [JsonProperty("index")]
        public long Index { get; set; }

        [JsonProperty("index2")]
        public long Index2 { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("odds")]
        public double Odds { get; set; }

        [JsonProperty("name")]
        public SourceNameClass Name { get; set; }

        [JsonProperty("sourceName", NullValueHandling = NullValueHandling.Ignore)]
        public SourceNameClass SourceName { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }
    }

    public partial class ParticipantBwin
    {
        [JsonProperty("participantId")]
        public long ParticipantId { get; set; }

        [JsonProperty("name")]
        public ParticipantName NameBwin { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }
    }

    public partial class Image
    {
        [JsonProperty("jersey")]
        public long Jersey { get; set; }

        [JsonProperty("rotateJersey")]
        public bool RotateJersey { get; set; }
    }

    public partial class ParticipantName
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("sign")]
        public string Sign { get; set; }

        [JsonProperty("short")]
        public string Short { get; set; }

        [JsonProperty("shortSign")]
        public string ShortSign { get; set; }
    }

    public partial class Sport
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public SourceNameClass Name { get; set; }
    }
}
