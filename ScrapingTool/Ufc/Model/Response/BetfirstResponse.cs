﻿namespace ScrapingTool.Ufc
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class BetfirstResponse
    {
        [JsonProperty("events")]
        public Event[] Events { get; set; }

        [JsonProperty("regions")]
        public Region[] Regions { get; set; }

        [JsonProperty("leagues")]
        public League[] Leagues { get; set; }

        [JsonProperty("markets")]
        public Market[] Markets { get; set; }
    }

    public partial class Event
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("sportId")]
        public long SportId { get; set; }

        [JsonProperty("sportOrder")]
        public long SportOrder { get; set; }

        [JsonProperty("sportName")]
        public string SportName { get; set; }

        [JsonProperty("eventName")]
        public string EventName { get; set; }

        [JsonProperty("betslipLine")]
        public string BetslipLine { get; set; }

        [JsonProperty("regionId")]
        public long RegionId { get; set; }

        [JsonProperty("regionCode")]
        public string RegionCode { get; set; }

        [JsonProperty("regionName")]
        public string RegionName { get; set; }

        [JsonProperty("leagueId")]
        public long LeagueId { get; set; }

        [JsonProperty("leagueName")]
        public string LeagueName { get; set; }

        [JsonProperty("leagueOrder")]
        public long LeagueOrder { get; set; }

        [JsonProperty("isTopLeague")]
        public bool IsTopLeague { get; set; }

        [JsonProperty("participants")]
        public Participant[] Participants { get; set; }

        [JsonProperty("marketGroups")]
        public MarketGroup[] MarketGroups { get; set; }

        [JsonProperty("totalMarketsCount")]
        public long TotalMarketsCount { get; set; }

        [JsonProperty("marketLinesCount")]
        public long MarketLinesCount { get; set; }

        [JsonProperty("startEventDate")]
        public DateTimeOffset StartEventDate { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("score")]
        public object Score { get; set; }

        [JsonProperty("isLive")]
        public bool IsLive { get; set; }

        [JsonProperty("isGoingLive")]
        public bool IsGoingLive { get; set; }

        [JsonProperty("liveGameState")]
        public object LiveGameState { get; set; }

        [JsonProperty("tags")]
        public object[] Tags { get; set; }

        [JsonProperty("metadata")]
        public EventMetadata Metadata { get; set; }

        [JsonProperty("isSuspended")]
        public bool IsSuspended { get; set; }

        [JsonProperty("isTeamSwap")]
        public bool IsTeamSwap { get; set; }

        [JsonProperty("media")]
        public object[] Media { get; set; }

        [JsonProperty("toteDetails")]
        public object ToteDetails { get; set; }
    }

    public partial class MarketGroup
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("order")]
        public long Order { get; set; }
    }

    public partial class EventMetadata
    {
        [JsonProperty("masterLeagueId")]
        public long MasterLeagueId { get; set; }
    }

    public partial class Participant
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("venueRole")]
        public string VenueRole { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("metadata")]
        public ParticipantMetadata Metadata { get; set; }
    }

    public partial class ParticipantMetadata
    {
        [JsonProperty("retailRotNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? RetailRotNumber { get; set; }
    }

    public partial class League
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sportId")]
        public long SportId { get; set; }

        [JsonProperty("sportName")]
        public string SportName { get; set; }

        [JsonProperty("regionId")]
        public long RegionId { get; set; }

        [JsonProperty("regionCode")]
        public string RegionCode { get; set; }

        [JsonProperty("masterLeagueId")]
        public object MasterLeagueId { get; set; }

        [JsonProperty("defaultOrder")]
        public long DefaultOrder { get; set; }

        [JsonProperty("isTopLeague")]
        public bool IsTopLeague { get; set; }

        [JsonProperty("liveFixturesTotalCount")]
        public long LiveFixturesTotalCount { get; set; }

        [JsonProperty("fixturesTotalCount")]
        public long FixturesTotalCount { get; set; }

        [JsonProperty("fixturesCount")]
        public long FixturesCount { get; set; }

        [JsonProperty("outrightsTotalCount")]
        public long OutrightsTotalCount { get; set; }

        [JsonProperty("tags")]
        public object Tags { get; set; }

        [JsonProperty("liveOutrightsTotalCount")]
        public long LiveOutrightsTotalCount { get; set; }

        [JsonProperty("metadata")]
        public LeagueMetadata Metadata { get; set; }
    }

    public partial class LeagueMetadata
    {
    }

    public partial class Market
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("betslipLine")]
        public string BetslipLine { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("marketType")]
        public MarketType MarketType { get; set; }

        [JsonProperty("leagueId")]
        public long LeagueId { get; set; }

        [JsonProperty("sportId")]
        public long SportId { get; set; }

        [JsonProperty("isLive")]
        public bool IsLive { get; set; }

        [JsonProperty("startDate")]
        public DateTimeOffset StartDate { get; set; }

        [JsonProperty("title")]
        public object Title { get; set; }

        [JsonProperty("participantMapping")]
        public object ParticipantMapping { get; set; }

        [JsonProperty("selections")]
        public Selection[] Selections { get; set; }

        [JsonProperty("isSuspended")]
        public bool IsSuspended { get; set; }

        [JsonProperty("eventId")]
        public long EventId { get; set; }

        [JsonProperty("liveData")]
        public object LiveData { get; set; }

        [JsonProperty("metadata")]
        public MarketMetadata Metadata { get; set; }

        [JsonProperty("tags")]
        public string[] Tags { get; set; }

        [JsonProperty("groups")]
        public long[] Groups { get; set; }

        [JsonProperty("inMarketGroups")]
        public InMarketGroup[] InMarketGroups { get; set; }

        [JsonProperty("castMarkets")]
        public object CastMarkets { get; set; }
    }

    public partial class InMarketGroup
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("sortingKey")]
        public long SortingKey { get; set; }
    }

    public partial class MarketType
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isCastMarket")]
        public bool IsCastMarket { get; set; }
    }

    public partial class MarketMetadata
    {
        [JsonProperty("sortingKey")]
        public long SortingKey { get; set; }
    }

    public partial class Selection
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("marketId")]
        public string MarketId { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("outcomeType")]
        public string OutcomeType { get; set; }

        [JsonProperty("group")]
        public long Group { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("betslipLine")]
        public string BetslipLine { get; set; }

        [JsonProperty("participantMapping")]
        public object ParticipantMapping { get; set; }

        [JsonProperty("displayOdds")]
        public DisplayOdds DisplayOdds { get; set; }

        [JsonProperty("trueOdds")]
        public double TrueOdds { get; set; }

        [JsonProperty("points")]
        public double? Points { get; set; }

        [JsonProperty("isDisabled")]
        public bool IsDisabled { get; set; }

        [JsonProperty("tags")]
        public string[] Tags { get; set; }

        [JsonProperty("metadata")]
        public SelectionMetadata Metadata { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public partial class DisplayOdds
    {
        [JsonProperty("american")]
        public string American { get; set; }

        [JsonProperty("decimal")]
        public string Decimal { get; set; }

        [JsonProperty("fractional")]
        public string Fractional { get; set; }

        [JsonProperty("hk")]
        public string Hk { get; set; }

        [JsonProperty("indo")]
        public string Indo { get; set; }

        [JsonProperty("malay")]
        public string Malay { get; set; }
    }

    public partial class SelectionMetadata
    {
        [JsonProperty("idSBTech")]
        public long IdSbTech { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }
    }

    public partial class Region
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("entityType")]
        public string EntityType { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("activeSports")]
        public object ActiveSports { get; set; }

        [JsonProperty("activeSportsCount")]
        public long ActiveSportsCount { get; set; }

        [JsonProperty("liveFixturesTotalCount")]
        public long LiveFixturesTotalCount { get; set; }

        [JsonProperty("fixturesTotalCount")]
        public long FixturesTotalCount { get; set; }

        [JsonProperty("fixturesCount")]
        public long FixturesCount { get; set; }

        [JsonProperty("outrightsTotalCount")]
        public long OutrightsTotalCount { get; set; }

        [JsonProperty("liveOutrightsTotalCount")]
        public long LiveOutrightsTotalCount { get; set; }
    }

}
