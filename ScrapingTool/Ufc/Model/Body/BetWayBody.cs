﻿using System;
using Newtonsoft.Json;
namespace ScrapingTool.Ufc
{


    public partial class BetWayBody
    {

        [JsonProperty("LanguageId")]
        public long LanguageId { get; set; }

        [JsonProperty("ClientTypeId")]
        public long ClientTypeId { get; set; }

        [JsonProperty("BrandId")]
        public long BrandId { get; set; }

        [JsonProperty("JurisdictionId")]
        public long JurisdictionId { get; set; }

        [JsonProperty("ClientIntegratorId")]
        public long ClientIntegratorId { get; set; }

        [JsonProperty("ExternalIds")]
        public long[] ExternalIds { get; set; }

        [JsonProperty("MarketCName")]
        public string MarketCName { get; set; }

        [JsonProperty("ScoreboardRequest")]
        public ScoreboardRequest ScoreboardRequest { get; set; }

        [JsonProperty("BrowserId")]
        public long BrowserId { get; set; }

        [JsonProperty("OsId")]
        public long OsId { get; set; }

        [JsonProperty("ApplicationVersion")]
        public string ApplicationVersion { get; set; }

        [JsonProperty("BrowserVersion")]
        public string BrowserVersion { get; set; }

        [JsonProperty("OsVersion")]
        public string OsVersion { get; set; }

        [JsonProperty("SessionId")]
        public object SessionId { get; set; }

        [JsonProperty("TerritoryId")]
        public long TerritoryId { get; set; }

        [JsonProperty("CorrelationId")]
        public Guid CorrelationId { get; set; }

        [JsonProperty("ViewName")]
        public string ViewName { get; set; }

        [JsonProperty("JourneyId")]
        public Guid JourneyId { get; set; }
    }

    public partial class ScoreboardRequest
    {
        [JsonProperty("ScoreboardType")]
        public long ScoreboardType { get; set; }

        [JsonProperty("IncidentRequest")]
        public IncidentRequest IncidentRequest { get; set; }
    }

    public partial class IncidentRequest
    {
    }
}
