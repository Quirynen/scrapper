﻿using Newtonsoft.Json;

namespace ScrapingTool.Ufc
{
    public partial class Bet777Body
    {
        [JsonProperty("eventState")]
        public string EventState { get; set; }

        [JsonProperty("eventTypes")]
        public string[] EventTypes { get; set; }

        [JsonProperty("ids")]
        public long[] Ids { get; set; }

        [JsonProperty("regionIds")]
        public long[] RegionIds { get; set; }

        [JsonProperty("marketTypeRequests")]
        public MarketTypeRequest[] MarketTypeRequests { get; set; }
    }

    public partial class MarketTypeRequest
    {
        [JsonProperty("sportIds")]
        public object[] SportIds { get; set; }

        [JsonProperty("marketTypeIds")]
        public string[] MarketTypeIds { get; set; }

        [JsonProperty("statement")]
        public string Statement { get; set; }
    }
}