﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace ScrapingTool.Ufc
{


    public partial class BwinBody
    {
        [JsonProperty("sportIds")]
        public long SportIds { get; set; }

        [JsonProperty("fixtureCategories")]
        public string FixtureCategories { get; set; }

        [JsonProperty("offerMapping")]
        public string OfferMapping { get; set; }

        [JsonProperty("offerCategories")]
        public string OfferCategories { get; set; }

        [JsonProperty("scoreboardMode")]
        public string ScoreboardMode { get; set; }

        [JsonProperty("marqueeRequest")]
        public MarqueeRequest MarqueeRequest { get; set; }

        [JsonProperty("fixtureTypes")]
        public string FixtureTypes { get; set; }

        [JsonProperty("showcaseRequest")]
        public ShowcaseRequest ShowcaseRequest { get; set; }
    }

    public partial class MarqueeRequest
    {
        [JsonProperty("marqueeData")]
        public object[] MarqueeData { get; set; }

        [JsonProperty("take")]
        public long Take { get; set; }
    }

    public partial class ShowcaseRequest
    {
        [JsonProperty("queries")]
        public object[] Queries { get; set; }
    }
}