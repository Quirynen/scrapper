﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using ScrapingTool.Repository.Model;

namespace ScrapingTool.Ufc
{
    public class Unibet_Ufc : WebSiteApiRules
    {

        HttpClient _client;
        List<MatchUfc> listMatch;
        string url;
        private UnibetResponse apiReponse;

        public Unibet_Ufc()
        {
            listMatch = new List<MatchUfc>();
            url = "https://eu-offering.kambicdn.org/offering/v2018/ubeu/listView/ufc_mma.json?lang=en_GB&market=NL&client_id=2&channel_id=1&ncid=1591553015899&useCombined=true";
            _client = Driver.Client.Instance;

            CallAPIAsync();
        }
        public void BuildMatchs()
        {
            try
            {
                foreach (var _event in apiReponse.Events)
                {
                    var odds = _event.BetOffers.Where(x => x.BetOfferType.Name == "Match");
                    var firstOdd = odds.FirstOrDefault().Outcomes.Where(x => x.Label == 1);
                    var secundOdd = odds.FirstOrDefault().Outcomes.Where(x => x.Label == 2);
                    double oddA = firstOdd.FirstOrDefault() != null
                        ? (double)firstOdd.FirstOrDefault().Odds / 1000
                        : throw new Exception(_event.Event.Name);
                    double oddB = secundOdd.FirstOrDefault() != null
                        ? (double)secundOdd.FirstOrDefault().Odds / 1000
                        : throw new Exception(_event.Event.Name);
                    string firstOpponent = _event.Event.HomeName;
                    string secundOpponent = _event.Event.AwayName;
                    DateTime start = _event.Event.Start.DateTime.ToLocalTime();
                    MatchUfc match = new MatchUfc(start, firstOpponent, secundOpponent, oddA, oddB);
                    listMatch.Add(match);
                }
                this.InsertMatch();
            }
            catch (Exception e)
            {
                Console.WriteLine("Problème dans Unibet UFC avec le match : " + e.Message);
            }

        }

        public async void CallAPIAsync()
        {
            var response = await getResponse();
            apiReponse = JsonConvert.DeserializeObject<UnibetResponse>(response);
            BuildMatchs();
        }

        public async Task<string> getResponse()
        {
            return await _client.GetAsync(url).Result.Content.ReadAsStringAsync();

        }

        public void InsertMatch()
        {
            try
            {
                UfcService.insertMatchUfc(listMatch, "Unibet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert UniBet Match", ex);
                throw new Exception();
            }
        }
    }
}
