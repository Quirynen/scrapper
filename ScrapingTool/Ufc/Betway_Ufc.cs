﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using ScrapingTool.Repository.Model;

namespace ScrapingTool.Ufc
{
    public class Betway_Ufc : WebSiteApiRules
    {
        HttpClient _client;
        List<MatchUfc> listMatch;
        string url;
        private BetWayResponse apiReponse;

        public Betway_Ufc()
        {
            listMatch = new List<MatchUfc>();
            url = "https://sports.betway.be/api/Events/V2/GetEvents";
            _client = Driver.Client.Instance;
            CallAPIAsync();
        }
        public void BuildMatchs()
        {
            foreach (var _event in apiReponse.Events)
            {
                var odds = apiReponse.Markets.Where(x => x.EventId == _event.Id).FirstOrDefault();
                if (odds != null)
                {
                    long idOddA = odds.Outcomes[0][0];
                    long idOddB = odds.Outcomes[0][1];
                    if (idOddA != null)
                    {
                        double oddA = apiReponse.Outcomes.Where(x => x.Id == idOddA).FirstOrDefault().OddsDecimal;
                        double oddB = apiReponse.Outcomes.Where(x => x.Id == idOddB).FirstOrDefault().OddsDecimal;
                        string firstOpponent = _event.HomeTeamName;
                        string secundOpponent = _event.AwayTeamName;
                        DateTime start = _event.StartEventDate.DateTime.ToLocalTime();
                        MatchUfc match = new MatchUfc(start, firstOpponent, secundOpponent, oddA, oddB);
                        listMatch.Add(match);
                    }
                }
            }
            this.InsertMatch();
        }

        public async void CallAPIAsync()
        {
            var response = await getResponse();
            apiReponse = JsonConvert.DeserializeObject<BetWayResponse>(response);
            BuildMatchs();
        }

        public async Task<string> getResponse()
        {
            BetWayBody body = new BetWayBody();
            body.ApplicationVersion = "";
            body.BrandId = 3;
            body.ClientTypeId = 2;
            body.LanguageId = 7;
            body.JurisdictionId = 1;
            body.ExternalIds = new long[]
            {
                5847570, 5847551, 5847552, 5847553, 5847554, 5847555, 5847556, 5847557, 5847568, 5866730, 5852055,
                5852054, 5847571, 5847572, 5866731, 5880335, 5880333, 5880344, 5847574, 5880334, 5852053, 5904570,
                5847573
            };
            body.MarketCName = "fight-winner";
            body.ScoreboardRequest = new ScoreboardRequest() { IncidentRequest = new IncidentRequest(), ScoreboardType = 3 };
            body.BrowserId = 3;
            body.OsId = 3;
            body.BrowserVersion = "83.0.4103.97";
            body.OsVersion = "NT 10.0";
            body.SessionId = null;
            body.TerritoryId = 21;
            body.CorrelationId = new Guid("fb169c74-f148-4a79-bfa9-9cb280fb260b");
            body.ViewName = "sports";
            body.JourneyId = new Guid("9c0f33b9-83aa-47d7-8175-24af6940184d");
            var httpContent = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            return await _client.PostAsync(url, httpContent).Result.Content.ReadAsStringAsync();
        }

        public void InsertMatch()
        {
            try
            {
                UfcService.insertMatchUfc(listMatch, "BetWay");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert BetWay Match", ex);
                throw new Exception();
            }
        }
    }
}
