﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using ScrapingTool.Repository.Model;

namespace ScrapingTool.Ufc
{
    public class Bwin_Ufc : WebSiteApiRules
    {

        HttpClient _client;
        List<MatchUfc> listMatch;
        string url;
        private BwinResponse apiReponse;

        public Bwin_Ufc()
        {
            listMatch = new List<MatchUfc>();
            url = "https://cds-api.bwin.be/bettingoffer/lobby/sport?x-bwin-accessid=NTE3MjUyZDUtNGU5Ni00MTkwLWJkMGQtMDhmOGViNGNiNmRk&country=BE&userCountry=BE&lang=fr";
            _client = Driver.Client.Instance;
            CallAPIAsync();
        }

        public void BuildMatchs()
        {
            foreach (var _event in apiReponse.Highlights)
            {

                double oddA = _event.Games.FirstOrDefault().Results[0].Odds;
                double oddB = _event.Games.FirstOrDefault().Results[1].Odds; ;
                string firstOpponent = _event.Games.FirstOrDefault().Results[0].Name.Value;
                string secundOpponent = _event.Games.FirstOrDefault().Results[1].Name.Value;
                DateTime start = _event.StartDate.DateTime.ToLocalTime();
                MatchUfc match = new MatchUfc(start, firstOpponent, secundOpponent, oddA, oddB);
                listMatch.Add(match);
            }
            this.InsertMatch();
        }

        public async void CallAPIAsync()
        {
            var response = await getResponse();
            apiReponse = JsonConvert.DeserializeObject<BwinResponse>(response);
            BuildMatchs();
        }

        public async Task<string> getResponse()
        {
            BwinBody body = new BwinBody();
            body.FixtureCategories = "Gridable,NonGridable,Other,Outrights";
            body.SportIds = 45;
            body.FixtureTypes = "Standard";
            body.OfferCategories = "Gridable,Other";
            body.OfferMapping = "Filtered";
            body.ScoreboardMode = "Slim";
            body.MarqueeRequest = new MarqueeRequest { MarqueeData = null, Take = 8 };
            body.ShowcaseRequest = new ShowcaseRequest { Queries = null };
            var httpContent = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

            return await _client.PostAsync(url, httpContent).Result.Content.ReadAsStringAsync();
        }

        public void InsertMatch()
        {
            try
            {
                UfcService.insertMatchUfc(listMatch, "bwin");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert Bwin Match", ex);
                throw new Exception();
            }
        }
    }
}

