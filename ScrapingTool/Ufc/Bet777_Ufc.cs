﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using ScrapingTool.Repository.Model;

namespace ScrapingTool.Ufc
{
    public class Bet777_Ufc : WebSiteApiRules
    {
        HttpClient _client;
        List<MatchUfc> listMatch;
        string url;
        private string authorizationToken;
        private BetfirstResponse apiReponse;

        public Bet777_Ufc()
        {
            listMatch = new List<MatchUfc>();
            url = "https://sbapi.sbtech.com/bet777/sportscontent/sportsbook/v1/Events/GetByLeagueId";
            _client = Driver.Client.Instance;
            authorizationToken =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTaXRlSWQiOjcyLCJTZXNzaW9uSWQiOiJkZWI5MGUzNi1kN2U0LTRjOTYtODExMC0wOWE5OWM1NzM3MTAiLCJuYmYiOjE1OTE2MDQ3NzYsImV4cCI6MTU5MjIwOTYwNiwiaWF0IjoxNTkxNjA0ODA2fQ.CnZkIJdpid93R7Ho_iLs1kp46v4jkOT9smluse8QJeE";
            CallAPIAsync();
        }
        public void BuildMatchs()
        {
            throw new NotImplementedException();
        }

        public async void CallAPIAsync()
        {
            var response = await getResponse();
            apiReponse = JsonConvert.DeserializeObject<BetfirstResponse>(response);
            BuildMatchs();
        }

        public async Task<string> getResponse()
        {
            Bet777Body body = new Bet777Body();
            body.EventState = "Mixed";
            body.EventTypes = new string[] { "Fixture", "AggregateFixture" };
            body.Ids = new long[] { 9034 };
            MarketTypeRequest marketTypeRequest = new MarketTypeRequest
            { MarketTypeIds = new string[] { "1_39", "2_39", "3_39", "1_0", "2_0", "3_0" }, Statement = "Include", SportIds = new Object[0] };
            body.MarketTypeRequests = new MarketTypeRequest[] { marketTypeRequest };
            body.RegionIds = new long[] { 20 };
            var httpContent = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorizationToken);
            return await _client.PostAsync(url, httpContent).Result.Content.ReadAsStringAsync();
        }

        public void InsertMatch()
        {
            try
            {
                UfcService.insertMatchUfc(listMatch, "Bet777");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert Bet777 Match", ex);
                throw new Exception();
            }
        }
    }
}
