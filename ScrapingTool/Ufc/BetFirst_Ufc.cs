﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using ScrapingTool.Repository.Model;

namespace ScrapingTool.Ufc
{
    public class BetFirst_Ufc : WebSiteApiRules
    {
        HttpClient _client;
        List<MatchUfc> listMatch;
        string url;
        private string authorizationToken;
        private BetfirstResponse apiReponse;

        public BetFirst_Ufc()
        {
            listMatch = new List<MatchUfc>();
            url = "https://sbapi.sbtech.com/betfirst/sportscontent/sportsbook/v1/Events/GetByLeagueId";
            _client = Driver.Client.Instance;
            authorizationToken =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTaXRlSWQiOjI4LCJTZXNzaW9uSWQiOiI3ZjQ0ZmQ1MC1hYjE0LTQ0ZjYtOWU4Ni1jMGM0NDk3ZDVjYmIiLCJuYmYiOjE1OTMwNzAwMzEsImV4cCI6MTU5MzY3NDg2MSwiaWF0IjoxNTkzMDcwMDYxfQ.TA-1MrTfuo011QE2WJmM1j688etN9PBoOeiehgoSnBs";
            CallAPIAsync();
        }
        public void BuildMatchs()
        {
            foreach (var _event in apiReponse.Events)
            {
                var odds = apiReponse.Markets.Where(x => x.EventId == _event.Id);
                Market market = null;
                foreach (var odd in odds)
                {
                    if (odd.Selections.Any(x => x.OutcomeType == "Home" || x.OutcomeType == "Away"))
                    {
                        market = odd;
                    }
                }

                if (market != null)
                {
                    double oddA = market.Selections.Where(x => x.OutcomeType == "Home").FirstOrDefault().TrueOdds;
                    double oddB = market.Selections.Where(x => x.OutcomeType == "Away").FirstOrDefault().TrueOdds;
                    string firstOpponent = _event.Participants[0].Name;
                    string secundOpponent = _event.Participants[1].Name;
                    DateTime start = _event.StartEventDate.DateTime.ToLocalTime();
                    MatchUfc match = new MatchUfc(start, firstOpponent, secundOpponent, oddA, oddB);
                    listMatch.Add(match);
                }

            }
            this.InsertMatch();
        }

        public async void CallAPIAsync()
        {
            var response = await getResponse();
            apiReponse = JsonConvert.DeserializeObject<BetfirstResponse>(response);
            BuildMatchs();
        }

        public async Task<string> getResponse()
        {
            Bet777Body body = new Bet777Body();
            body.EventState = "Mixed";
            body.EventTypes = new string[] { "Fixture", "AggregateFixture" };
            body.Ids = new long[] { 9034 };
            MarketTypeRequest marketTypeRequest = new MarketTypeRequest
            { MarketTypeIds = new string[] { "1_39", "2_39", "3_39", "1_0", "2_0", "3_0" }, Statement = "Include", SportIds = new Object[0] };
            body.MarketTypeRequests = new MarketTypeRequest[] { marketTypeRequest };
            body.RegionIds = new long[] { 20 };
            var httpContent = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorizationToken);
            return await _client.PostAsync(url, httpContent).Result.Content.ReadAsStringAsync();
        }

        public void InsertMatch()
        {
            try
            {
                UfcService.insertMatchUfc(listMatch, "betfirst");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert BetFirst Match", ex);
                throw new Exception();
            }
        }
    }
}
