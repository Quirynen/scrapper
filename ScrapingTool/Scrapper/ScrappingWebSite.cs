﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using ScrapingTool.Driver;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace ScrapingTool.Scrapper
{
    public class ScrappingWebSite 
    {
        public ChromeDriver driver { get; set; }
        WebDriverWait wait;
        HtmlDocument doc;
        public ScrappingWebSite(string url,  string[] valuesToWait,string elementToScroll, int scrollPoints)
        {
            driver = SingletonDriver.Instance;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60.00));
            this.NavigateAndWait(url, valuesToWait, elementToScroll,scrollPoints);
        }
        public void ScrollToBottom(string elementToScroll, int scrollPoints)
        {
            if(elementToScroll!=null && scrollPoints > 0)
            {
                IWebElement element = driver.FindElementByXPath(elementToScroll);
                try
                {
                    Actions dragger = new Actions(driver);
                    // drag downwards
                    int numberOfPixelsToDragTheScrollbarDown = 100;
                    for (int i = 10; i < scrollPoints; i = i + numberOfPixelsToDragTheScrollbarDown)
                    {
                        dragger.MoveToElement(element).ClickAndHold().MoveByOffset(0, numberOfPixelsToDragTheScrollbarDown).Release(element).Build().Perform();
                    }
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception survenue dans le scroll :" + e);
                }
            }
           
        }
        // Navigate to the url and wait for values to be availables
        private void NavigateAndWait(string url, string[] valuesToWait, string elementToScroll, int scrollPoints)
        {
            driver.Navigate().GoToUrl(url);
            try
            {
                foreach (string waitingValue in valuesToWait)
                {
                    wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(waitingValue)));
                }
                ScrollToBottom(elementToScroll, scrollPoints);
            }catch(Exception ex)
            {
                Console.WriteLine("Problème dans l'attente des valeurs : " + ex);
            }
            loadHtml();
        }
        // Load the html
        private void loadHtml()
        {
            doc = new HtmlDocument();
            doc.LoadHtml(driver.PageSource);
        }

        //Retrieve data from the html
        public HtmlNodeCollection retrieveData(string xpathToGet)
        {
            try
            {
                return doc.DocumentNode.SelectNodes(xpathToGet);
            }catch(Exception ex)
            {
                Console.WriteLine("Problème pour trouver le xpAth : " + xpathToGet + "Exception : "+ex);
                throw new Exception();
            }
        }

    }
}
