﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ScrapingTool.Scrapper
{
    public class BwinScrap : ScrappingWebsiteRules
    {
        ScrappingWebSite _scrapper;
      
        List<MatchFoot> listMatchFoot;
        //Scapper bwin
        public BwinScrap()
        {
             _scrapper = new ScrappingWebSite("https://sports.bwin.be/fr/sports/football-4/demain", new string[] { "//div[contains(concat(' ', normalize-space(@class), ' '), ' participant ')]" }, "/html/body/vn-app/vn-dynamic-layout-single-slot[3]/vn-main/main/div/ms-main/div/div[3]/div", 1000);
            BuildMatchs();
        }
       
        public void BuildMatchs()
        {
            HtmlNodeCollection matchsGrid = this._scrapper.retrieveData("//*[contains(@class, 'grid-event-wrapper')]");
            listMatchFoot = new List<MatchFoot>();
            for (int i = 0; i <= matchsGrid.Count - 1; i++)
            {
                //On récupère les cotes selon le format : 1 1 1 1 1 /// Seulement les trois premières sont à utiliser
                try
                {
                    HtmlNodeCollection time = this.scrapHoursGame(".//*[contains(@class, 'timer-badge starting-time')]", matchsGrid[i]);
                    HtmlNodeCollection participants = this.scrapTeams(".//div[contains(concat(' ', normalize-space(@class), ' '), ' participant ')]", matchsGrid[i]);
                    HtmlNodeCollection odds = this.scrapOdds(".//*[contains(@class, 'option option-indicator')]", matchsGrid[i]);
                    if(time!=null && participants !=null && odds != null)
                    {
                        MatchFoot match = new MatchFoot(transformTextToDate(time[0].InnerText), participants[0].InnerText, participants[1].InnerText, double.Parse(odds[0].InnerText, CultureInfo.InvariantCulture), double.Parse(odds[1].InnerText, CultureInfo.InvariantCulture), double.Parse(odds[2].InnerText, CultureInfo.InvariantCulture));
                        listMatchFoot.Add(match);
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine("Exception" +ex +"Index est "+i);
                }

            }
            this.InsertMatch();
        }

        public void InsertMatch()
        {
            try
            {
                MatchFootService.insertMatchFoot(listMatchFoot, "Bwin");
            }catch(Exception ex)
            {
                Console.WriteLine("Problème insert Bwin Match", ex);
                throw new Exception();
            }
        }

        public HtmlNodeCollection scrapHoursGame(string xpath, HtmlNode nodes)
        {
            try
            {
                return nodes.SelectNodes(xpath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème scrapHoursGame Bwin Match", ex);
                throw new Exception();
            }
        }

        public HtmlNodeCollection scrapOdds(string xpath, HtmlNode nodes)
        {
            try
            {
                return nodes.SelectNodes(xpath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème scrapOdds Bwin Match", ex);
                throw new Exception();
            }
        }

        public HtmlNodeCollection scrapTeams(string xpath, HtmlNode nodes)
        {
            try
            {
                return nodes.SelectNodes(xpath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème scrapTeams Bwin Match", ex);
                throw new Exception();
            }

        }

      
        private DateTime transformTextToDate(string s)
        {
            try
            {
                s=s.Replace("Demain / ", "");
                s.Trim();
                string[] stDates = s.Split(':');
                return new DateTime().AddDays(1).AddHours(Double.Parse(stDates[0])).AddMinutes(Double.Parse(stDates[1]));
            }catch(Exception ex)
            {
                Console.WriteLine("Problème dans la création d'une date " +ex);
                throw new Exception();
            }

        }
    }
}
