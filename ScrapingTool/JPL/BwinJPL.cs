﻿using Newtonsoft.Json;
using ScrapingTool.Interface;
using ScrapingTool.Model;
using ScrapingTool.Repository.DataActions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static ScrapingTool.Model.BwinResponse;

namespace ScrapingTool.JPL
{
    public class BwinJPL : WebSiteApiRules
    {
        HttpClient _client;
        string response;
        List<MatchFoot> listMatch;
        string url;
        BwinResp apiReponse;
        public BwinJPL()
        {
            listMatch = new List<MatchFoot>();
            url = constructUrl();
            _client = Driver.Client.Instance;
            CallAPIAsync();
        }

        private string constructUrl()
        {
            return "https://cds-api.bwin.be/bettingoffer/fixtures?x-bwin-accessid=NTE3MjUyZDUtNGU5Ni00MTkwLWJkMGQtMDhmOGViNGNiNmRk&lang=fr&country=BE&userCountry=BE&fixtureTypes=Standard&state=Latest&skip=0&take=50&offerMapping=Filtered&offerCategories=Gridable&fixtureCategories=Gridable,NonGridable,Other&sortBy=Tags&sportIds=4&regionIds=35&competitionIds=16409";
        }

        public void BuildMatchs()
        {
            foreach (var _event in apiReponse.fixtures)
            {
                if (_event.games.Count > 0 && _event.games[0].results.Count >= 3)
                {
                    double oddA = _event.games[0].results[0].odds;
                    double oddB = _event.games[0].results[2].odds; ;
                    double oddX = _event.games[0].results[1].odds; ;
                    string teamA = _event.participants[0].name.value;
                    string teamB = _event.participants[1].name.value;
                    DateTime start = _event.startDate.ToLocalTime();
                    MatchFoot match = new MatchFoot(start, teamA, teamB, oddA, oddX, oddB);
                    listMatch.Add(match);
                }

            }
            this.InsertMatch();
        }
        public async Task<string> getResponse()
        {
            return await _client.GetAsync(url).Result.Content.ReadAsStringAsync();

        }
        public async void CallAPIAsync()
        {
            response = await getResponse();
            apiReponse = JsonConvert.DeserializeObject<BwinResp>(response);
            BuildMatchs();
        }

        public void InsertMatch()
        {
            try
            {
                MatchFootService.insertMatchFoot(listMatch, "Bwin");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problème insert Bwin Match", ex);
                throw new Exception();
            }
        }


    }
}
