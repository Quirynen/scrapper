﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ScrapingTool.Repository.Model
{
    public partial class scrappingdataContext : DbContext
    {
        public scrappingdataContext()
        {
        }

        public scrappingdataContext(DbContextOptions<scrappingdataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Matchfoot> Matchfoot { get; set; }
        public virtual DbSet<Matchtennis> Matchtennis { get; set; }
        public virtual DbSet<UfcMatch> ufcMatch { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;password=root;database=scrappingdata");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Matchfoot>(entity =>
            {
                entity.HasKey(e => e.IdmatchFoot);

                entity.ToTable("matchfoot", "scrappingdata");

                entity.Property(e => e.IdmatchFoot)
                    .HasColumnName("idmatchFoot")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Heure).HasColumnName("heure");

                entity.Property(e => e.OddA).HasColumnName("oddA");

                entity.Property(e => e.OddB).HasColumnName("oddB");

                entity.Property(e => e.OddX).HasColumnName("oddX");

                entity.Property(e => e.SiteOddA)
                    .HasColumnName("siteOddA")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SiteOddB)
                    .HasColumnName("siteOddB")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SiteOddC)
                    .HasColumnName("siteOddC")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.TeamA)
                    .IsRequired()
                    .HasColumnName("teamA")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.TeamB)
                    .IsRequired()
                    .HasColumnName("teamB")
                    .HasMaxLength(445)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Matchtennis>(entity =>
            {
                entity.HasKey(e => e.Idmatchtennis);

                entity.ToTable("matchtennis", "scrappingdata");

                entity.Property(e => e.Idmatchtennis)
                    .HasColumnName("idmatchtennis")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Heure).HasColumnName("heure");

                entity.Property(e => e.OddA)
                    .IsRequired()
                    .HasColumnName("oddA")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.OddB)
                    .IsRequired()
                    .HasColumnName("oddB")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.PlayerA)
                    .IsRequired()
                    .HasColumnName("playerA")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.PlayerB)
                    .IsRequired()
                    .HasColumnName("playerB")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.SiteOddA)
                    .HasColumnName("siteOddA")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SiteOddB)
                    .HasColumnName("siteOddB")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<UfcMatch>(entity =>
            {
                entity.HasKey(e => e.id);

                entity.ToTable("ufcmatch", "scrappingdata");

                entity.Property(e => e.id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Heure).HasColumnName("heure");

                entity.Property(e => e.OddA)
                    .IsRequired()
                    .HasColumnName("oddA")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.OddB)
                    .IsRequired()
                    .HasColumnName("oddB")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.FirstOpponent)
                    .IsRequired()
                    .HasColumnName("FirstOpponent")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.SecundOpponent)
                    .IsRequired()
                    .HasColumnName("SecundOpponent")
                    .HasMaxLength(445)
                    .IsUnicode(false);

                entity.Property(e => e.SiteOddA)
                    .HasColumnName("siteOddA")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SiteOddB)
                    .HasColumnName("siteOddB")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });
        }
    }
}
