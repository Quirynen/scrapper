﻿using System;
using System.Collections.Generic;

namespace ScrapingTool.Repository.Model
{
    public partial class Matchfoot
    {
        public int IdmatchFoot { get; set; }
        public DateTime Heure { get; set; }
        public string TeamA { get; set; }
        public string TeamB { get; set; }
        public double OddA { get; set; }
        public double OddX { get; set; }
        public double OddB { get; set; }
        public string SiteOddA { get; set; }
        public string SiteOddB { get; set; }
        public string SiteOddC { get; set; }
    }
}
