﻿using System;
using System.Collections.Generic;

namespace ScrapingTool.Repository.Model
{
    public partial class Matchtennis
    {
        public int Idmatchtennis { get; set; }
        public string PlayerA { get; set; }
        public string PlayerB { get; set; }
        public string OddA { get; set; }
        public string OddB { get; set; }
        public DateTime Heure { get; set; }
        public string SiteOddA { get; set; }
        public string SiteOddB { get; set; }
    }
}
