﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Repository.Model
{
    public partial class UfcMatch
    {
        public int id { get; set; }
        public DateTime Heure { get; set; }
        public string FirstOpponent { get; set; }
        public string SecundOpponent { get; set; }
        public double OddA { get; set; }
        public double OddB { get; set; }
        public string SiteOddA { get; set; }
        public string SiteOddB { get; set; }
    }
}
