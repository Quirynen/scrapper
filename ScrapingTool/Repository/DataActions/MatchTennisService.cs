﻿using ScrapingTool.Model;
using ScrapingTool.Repository.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Repository.DataActions
{
    public class MatchTennisService
    {
        public void insertMatchTennis(MatchTennis match)
        {
            using (var context = new scrappingdataContext())
            {
                // select match avec +- les mm équipes
                Matchtennis mtchTennis = new Matchtennis()
                {
                    Heure = match.Heure,
                    OddA = match.OddA,
                    OddB = match.OddB,
                    PlayerA = match.PlayerA,
                    PlayerB = match.PlayerB
                };
                context.Matchtennis.Add(mtchTennis);
                context.SaveChanges();
            }

        }
    }
}
