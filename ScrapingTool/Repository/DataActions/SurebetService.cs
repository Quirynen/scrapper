﻿using ScrapingTool.Model;
using ScrapingTool.Repository.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ScrapingTool.Repository.DataActions
{
    public class SurebetService
    {
        public static void GetUfcSurebet()
        {
            using (var context = new scrappingdataContext())
            {
                var listMatch = from surebet in context.ufcMatch
                                where ((1 / surebet.OddA) + (1 / surebet.OddB) + (1 / surebet.OddB)) < 1
                                select surebet;
                foreach (var match in listMatch)
                {
                    double pourcentage = (1 - ((1 / match.OddA) + (1 / match.OddB) + (1 / match.OddB))) * 100;
                    Console.WriteLine("Il vous faut parier sur : ");
                    Console.WriteLine("-------------------------------");
                    Console.WriteLine("Le match entre : " + match.FirstOpponent + " vs " + match.SecundOpponent);
                    Console.WriteLine("Site : " + match.SiteOddA + " || Avec la code de : " + match.OddA);
                    Console.WriteLine("Site : " + match.SiteOddB + " || Avec la code de : " + match.OddB);
                    Console.WriteLine(" Avec un Bénéfice net de :::::: " + pourcentage + " %");
                    Console.WriteLine("-------------------------------");
                    Console.WriteLine("-------------------------------");
                    Console.WriteLine("-------------------------------");
                    Console.WriteLine("-------------------------------");
                }
            }
        }
    }
}
