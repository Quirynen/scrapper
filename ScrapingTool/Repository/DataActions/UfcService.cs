﻿using ScrapingTool.Repository.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using ScrapingTool.Model;

namespace ScrapingTool.Repository.DataActions
{
    public class UfcService
    {
        public static void insertMatchUfc(List<MatchUfc> matchList, string site)
        {
            using (var context = new scrappingdataContext())
            {

                foreach (var match in matchList)
                {
                    UfcMatch ufcMatch = new UfcMatch()
                    {
                        Heure = match.Heure,
                        OddA = match.OddA,
                        OddB = match.OddB,
                        FirstOpponent = match.FirstOpponent,
                        SecundOpponent = match.SecundOpponent,
                        SiteOddA = site,
                        SiteOddB = site
                    };
                    try
                    {
                        string nameFirst = ufcMatch.FirstOpponent.Replace(" ", "");
                        nameFirst = nameFirst.Replace("\"", "");
                        string nameSecund = ufcMatch.SecundOpponent.Replace(" ", "");
                        nameSecund = nameSecund.Replace("\"", "");
                        //On récupère les noms génériques pour toujours stocker les noms de la première colonne
                        string firstOpponent = ReadCsvToGetGenericName(site, nameFirst);
                        string secundOpponent = ReadCsvToGetGenericName(site, nameSecund);

                        var resultQuery = context.ufcMatch.Where(x =>
                            x.FirstOpponent == firstOpponent && x.SecundOpponent == secundOpponent
                                                             && x.Heure.DayOfYear == ufcMatch.Heure.DayOfYear);
                        ufcMatch.FirstOpponent = firstOpponent;
                        ufcMatch.SecundOpponent = secundOpponent;
                        if (firstOpponent == "NotFound" || secundOpponent == "NotFound")
                        {
                            throw new Exception("Bug dans le name retrieve pour le site : " + site);
                        }

                        if (resultQuery.FirstOrDefault() != null)
                        {
                            //Update des cotes

                            UpdateCotes(resultQuery.FirstOrDefault(), ufcMatch, site);
                        }
                        else
                        {
                            context.ufcMatch.Add(ufcMatch);
                            context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Problème dans le traitement du match : " + match.FirstOpponent + " VS " + match.SecundOpponent + " avec le message suivant : " + ex.Message);
                    }

                }
            }

        }

        private static string ReadCsvToGetGenericName(string site, string nameToRetrieve)
        {
            int siteIndex = 0;
            switch (site)
            {
                case "Unibet":
                    siteIndex = 0;
                    break;
                case "betway":
                    siteIndex = 1;
                    break;
                case "bet777":
                    siteIndex = 2;
                    break;
                case "betfirst":
                    siteIndex = 3;
                    break;
                case "bwin":
                    siteIndex = 4;
                    break;
                default:
                    siteIndex = 0;
                    break;
            }
            using (var reader = new StreamReader("C:\\Users\\lquirynen\\source\\repos\\scrapper\\ScrapingTool\\Ufc\\conv_names.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    string name = csv.GetField<string>(siteIndex).Replace(" ", "");

                    if (name.Equals(nameToRetrieve))
                    {
                        return csv.GetField<string>(0);
                    }
                }
            }

            return "NotFound";
        }
        /// <summary>
        /// Update les cotes si elles sont supérieures
        /// </summary>
        private static void UpdateCotes(UfcMatch matchAlreadyDb, UfcMatch matchToCompare, string site)
        {
            using (var context = new scrappingdataContext())
            {
                if (matchAlreadyDb.OddA < matchToCompare.OddA && !matchAlreadyDb.SiteOddA.Equals(matchToCompare.SiteOddA))
                {
                    Console.WriteLine("Match avec cotes updates :" + matchAlreadyDb.FirstOpponent + " vs " + matchAlreadyDb.SecundOpponent);
                    matchAlreadyDb.SiteOddA = site; //Site pour teamA
                    matchAlreadyDb.OddA = matchToCompare.OddA;
                    context.ufcMatch.Update(matchAlreadyDb);
                    context.SaveChanges();
                }
                if (matchAlreadyDb.OddB < matchToCompare.OddB && !(matchAlreadyDb.SiteOddB.Equals(matchToCompare.SiteOddB)))
                {
                    Console.WriteLine("Match avec cotes updates :" + matchAlreadyDb.FirstOpponent + " vs " + matchAlreadyDb.SecundOpponent);
                    matchAlreadyDb.SiteOddB = site; // site pour teamB
                    matchAlreadyDb.OddB = matchToCompare.OddB;
                    context.ufcMatch.Update(matchAlreadyDb);
                    context.SaveChanges();
                }

            }
        }
    }
}
