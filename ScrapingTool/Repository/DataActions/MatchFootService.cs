﻿using ScrapingTool.Model;
using ScrapingTool.Repository.Model;
using System;
using System.Collections.Generic;
using System.Text;
using NinjaNye.SearchExtensions.Levenshtein;
using System.Linq;

namespace ScrapingTool.Repository.DataActions
{
    public static class MatchFootService
    {
        public static void insertMatchFoot(List<MatchFoot> matchList, string site)
        {
            using (var context = new scrappingdataContext())
            {
                foreach(MatchFoot match in matchList)
                {
                    Matchfoot mtchFoot = new Matchfoot()
                    {
                        Heure = match.Heure,
                        OddA = match.OddA,
                        OddB = match.OddB,
                        OddX = match.OddX,
                        TeamA = match.TeamA,
                        TeamB = match.TeamB,
                        SiteOddA = site,
                        SiteOddB = site,
                        SiteOddC=site
                    };
                    try
                    {
                        var result = context.Matchfoot.LevenshteinDistanceOf(x => x.TeamA)
        .ComparedTo(match.TeamA).Where(x => x.Distance<=4);
                        var result2 = context.Matchfoot.LevenshteinDistanceOf(x => x.TeamB)
        .ComparedTo(match.TeamB).Where(x => x.Distance <= 4);
                        var res = result.Where(x => (x.Item.Heure.Hour == match.Heure.Hour) && (x.Item.Heure.Minute == match.Heure.Minute));
                        var res2 = result2.Where(x => (x.Item.Heure.Hour == match.Heure.Hour) && (x.Item.Heure.Minute == match.Heure.Minute));
                        if (res.FirstOrDefault() != null  && res2.FirstOrDefault() !=null)
                        {
                            UpdateCotes(res.FirstOrDefault().Item, mtchFoot,site);
                        }
                        else
                        {
                            context.Matchfoot.Add(mtchFoot);
                            context.SaveChanges();
                        }
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine("Problème dans levenshtein : " + ex);
                    }               
                   
                }             
            }

        }
        /// <summary>
        /// Update les cotes si elles sont supérieures
        /// </summary>
        private static void UpdateCotes(Matchfoot matchAlreadyDb, Matchfoot matchToCompare, string site)
        {
            using (var context = new scrappingdataContext())
            {
                if (matchAlreadyDb.OddA < matchToCompare.OddA && !matchAlreadyDb.SiteOddA.Equals( matchToCompare.SiteOddA))
                {
                    Console.WriteLine("Match avec cotes updates :" + matchAlreadyDb.TeamA + " vs " + matchAlreadyDb.TeamB);
                    matchAlreadyDb.SiteOddA = site; //Site pour teamA
                    matchAlreadyDb.OddA = matchToCompare.OddA;
                    context.Matchfoot.Update(matchAlreadyDb);
                    context.SaveChanges();
                }
                if (matchAlreadyDb.OddB < matchToCompare.OddB && !(matchAlreadyDb.SiteOddB.Equals( matchToCompare.SiteOddB)))
                {
                    Console.WriteLine("Match avec cotes updates :" + matchAlreadyDb.TeamA + " vs " + matchAlreadyDb.TeamB);
                    matchAlreadyDb.SiteOddC = site; // site pour teamB
                    matchAlreadyDb.OddB = matchToCompare.OddB;
                    context.Matchfoot.Update(matchAlreadyDb);
                    context.SaveChanges();
                }
                if (matchAlreadyDb.OddX < matchToCompare.OddX && !(matchAlreadyDb.SiteOddC.Equals( matchToCompare.SiteOddC)))
                {
                    Console.WriteLine("Match avec cotes updates :" + matchAlreadyDb.TeamA + " vs " + matchAlreadyDb.TeamB);
                    matchAlreadyDb.SiteOddB = site; // site pour match nul
                    matchAlreadyDb.OddX = matchToCompare.OddX;
                    context.Matchfoot.Update(matchAlreadyDb);
                    context.SaveChanges();
                }
            }
        }
    }
}
