﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ScrapingTool.Driver
{
    public class Client
    {
        private static readonly HttpClient client = new HttpClient();
        static Client()
        {
        }

        private Client()
        {

        }

        public static HttpClient Instance
        {
            get
            {
                return client;
            }
        }
    }
}
