﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace ScrapingTool.Driver
{
    public class SingletonDriver
    {
        private static readonly ChromeDriver driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
        static SingletonDriver()
        {
        }

        private SingletonDriver()
        {

        }

        public static ChromeDriver Instance
        {
            get
            {
                return driver;
            }
        }
    }
}
