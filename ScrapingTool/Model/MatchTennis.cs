﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Model
{
    public class MatchTennis
    {
        public string PlayerA { get; set; }
        public string PlayerB { get; set; }
        public string OddA { get; set; }
        public string OddB { get; set; }
       public DateTime Heure { get; set; }
    }
}
