﻿using System;

namespace ScrapingTool.Model
{
    public class MatchUfc
    {
        public DateTime Heure { get; set; }
        public string FirstOpponent { get; set; }
        public string SecundOpponent { get; set; }
        public double OddA { get; set; }
        public double OddB { get; set; }

        public MatchUfc(DateTime heure, string firstOpponent, string secundOpponent, double oddA, double oddB)
        {
            Heure = heure;
            FirstOpponent = firstOpponent;
            SecundOpponent = secundOpponent;
            OddA = oddA;
            OddB = oddB;
        }

        public override string ToString()
        {
            return "Match du jour en Ufc est : " + FirstOpponent.ToString() + " VS " + SecundOpponent.ToString() +
                   " à " + Heure.Hour.ToString() + ":" + Heure.Minute.ToString();
        }
    }
}
