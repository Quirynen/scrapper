﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Model
{
    public class BetFirstResponse
    {
        public class Metadata
        {
        }

        public class Participant
        {
            public string id { get; set; }
            public string name { get; set; }
            public string venueRole { get; set; }
            public string country { get; set; }
            public Metadata metadata { get; set; }
        }

        public class MarketGroup
        {
            public string id { get; set; }
            public string name { get; set; }
            public int order { get; set; }
        }

        public class Metadata2
        {
            public string masterLeagueId { get; set; }
            public string numberOfParts { get; set; }
            public string secondsInOnePart { get; set; }
        }

        public class Event
        {
            public string id { get; set; }
            public string entityType { get; set; }
            public string type { get; set; }
            public string sportId { get; set; }
            public int sportOrder { get; set; }
            public string sportName { get; set; }
            public string eventName { get; set; }
            public string betslipLine { get; set; }
            public string regionId { get; set; }
            public string regionCode { get; set; }
            public string regionName { get; set; }
            public string leagueId { get; set; }
            public string leagueName { get; set; }
            public int leagueOrder { get; set; }
            public bool isTopLeague { get; set; }
            public List<Participant> participants { get; set; }
            public List<MarketGroup> marketGroups { get; set; }
            public int totalMarketsCount { get; set; }
            public int marketLinesCount { get; set; }
            public DateTime startEventDate { get; set; }
            public string status { get; set; }
            public object score { get; set; }
            public bool isLive { get; set; }
            public bool isGoingLive { get; set; }
            public object liveGameState { get; set; }
            public List<object> tags { get; set; }
            public Metadata2 metadata { get; set; }
            public bool isSuspended { get; set; }
            public bool isTeamSwap { get; set; }
            public List<object> media { get; set; }
            public object toteDetails { get; set; }
        }

        public class Region
        {
            public string id { get; set; }
            public string entityType { get; set; }
            public string code { get; set; }
            public string name { get; set; }
            public object activeSports { get; set; }
            public int activeSportsCount { get; set; }
            public int liveFixturesTotalCount { get; set; }
            public int fixturesTotalCount { get; set; }
            public int fixturesCount { get; set; }
            public int outrightsTotalCount { get; set; }
            public int liveOutrightsTotalCount { get; set; }
        }

        public class Metadata3
        {
        }

        public class League
        {
            public string id { get; set; }
            public string entityType { get; set; }
            public string name { get; set; }
            public string sportId { get; set; }
            public string sportName { get; set; }
            public string regionId { get; set; }
            public string regionCode { get; set; }
            public object masterLeagueId { get; set; }
            public int defaultOrder { get; set; }
            public bool isTopLeague { get; set; }
            public int liveFixturesTotalCount { get; set; }
            public int fixturesTotalCount { get; set; }
            public int fixturesCount { get; set; }
            public int outrightsTotalCount { get; set; }
            public object tags { get; set; }
            public int liveOutrightsTotalCount { get; set; }
            public Metadata3 metadata { get; set; }
        }

        public class MarketType
        {
            public string id { get; set; }
            public string name { get; set; }
            public bool isCastMarket { get; set; }
        }

        public class DisplayOdds
        {
            public string american { get; set; }
            public string @decimal { get; set; }
            public string fractional { get; set; }
            public string hk { get; set; }
            public string indo { get; set; }
            public string malay { get; set; }
        }

        public class Metadata4
        {
            public string idSBTech { get; set; }
            public string type { get; set; }
        }

        public class Selection
        {
            public string id { get; set; }
            public string marketId { get; set; }
            public string entityType { get; set; }
            public string outcomeType { get; set; }
            public int group { get; set; }
            public string name { get; set; }
            public string title { get; set; }
            public string betslipLine { get; set; }
            public object participantMapping { get; set; }
            public DisplayOdds displayOdds { get; set; }
            public double trueOdds { get; set; }
            public double? points { get; set; }
            public bool isDisabled { get; set; }
            public List<object> tags { get; set; }
            public Metadata4 metadata { get; set; }
            public string status { get; set; }
        }

        public class Metadata5
        {
        }

        public class InMarketGroup
        {
            public string id { get; set; }
            public int sortingKey { get; set; }
        }

        public class Market
        {
            public string id { get; set; }
            public string name { get; set; }
            public string betslipLine { get; set; }
            public string entityType { get; set; }
            public MarketType marketType { get; set; }
            public string leagueId { get; set; }
            public string sportId { get; set; }
            public bool isLive { get; set; }
            public DateTime startDate { get; set; }
            public string title { get; set; }
            public object participantMapping { get; set; }
            public List<Selection> selections { get; set; }
            public bool isSuspended { get; set; }
            public string eventId { get; set; }
            public object liveData { get; set; }
            public Metadata5 metadata { get; set; }
            public List<string> tags { get; set; }
            public List<string> groups { get; set; }
            public List<InMarketGroup> inMarketGroups { get; set; }
            public object castMarkets { get; set; }
        }

        public class BetfirstResp
        {
            public List<Event> events { get; set; }
            public List<Region> regions { get; set; }
            public List<League> leagues { get; set; }
            public List<Market> markets { get; set; }
        }
    }
}
