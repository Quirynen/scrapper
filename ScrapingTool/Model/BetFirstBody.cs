﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Model
{
    public class BetFirstBody
    {
        public class DateRange
        {
            public DateTime from { get; set; }
            public string timeRange { get; set; }
        }

        public class MarketTypeRequest
        {
            public List<string> marketTypeIds { get; set; }
            public List<string> sportIds { get; set; }
            public string statement { get; set; }
        }

        public class Pagination
        {
            public int skip { get; set; }
            public int top { get; set; }
        }

        public class RootObject
        {
            public DateRange dateRange { get; set; }
            public string eventState { get; set; }
            public List<object> eventTags { get; set; }
            public List<string> eventTypes { get; set; }
            public List<string> excludeRegionIds { get; set; }
            public List<string> ids { get; set; }
            public string leagueState { get; set; }
            public List<MarketTypeRequest> marketTypeRequests { get; set; }
            public Pagination pagination { get; set; }
        }
    }
}
