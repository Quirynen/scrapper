﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Model
{
    public class MatchFoot
    {
        public DateTime Heure { get; set; }
        public string TeamA { get; set; }
        public string TeamB { get; set; }
        public double OddA { get; set; }
        public double OddX { get; set; }
        public double OddB { get; set; }

        public MatchFoot(DateTime heure, string teamA, string teamB, double oddA, double oddX, double oddB)
        {
            Heure = heure;
            TeamA = teamA;
            TeamB = teamB;
            OddA = oddA;
            OddX = oddX;
            OddB = oddB;
        }

        public override string ToString()
        {
            return "Match du jour : "+Heure.ToShortTimeString()+"/"+TeamA+"/"+TeamB+"  " + OddA+" X : " +OddX+ "    "+OddB;
        }
    }
}
