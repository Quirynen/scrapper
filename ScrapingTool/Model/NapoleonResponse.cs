﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Model
{
    public class NapoleonResponse
    {
        public class Path
        {
            public int id { get; set; }
            public string name { get; set; }
            public string englishName { get; set; }
            public string termKey { get; set; }
        }

        public class Home
        {
            public string shirtColor1 { get; set; }
            public string shirtColor2 { get; set; }
        }

        public class Away
        {
            public string shirtColor1 { get; set; }
            public string shirtColor2 { get; set; }
        }

        public class TeamColors
        {
            public Home home { get; set; }
            public Away away { get; set; }
        }

        public class Event2
        {
            public int id { get; set; }
            public string name { get; set; }
            public string englishName { get; set; }
            public string homeName { get; set; }
            public string awayName { get; set; }
            public DateTime start { get; set; }
            public string group { get; set; }
            public int groupId { get; set; }
            public List<Path> path { get; set; }
            public int nonLiveBoCount { get; set; }
            public int liveBoCount { get; set; }
            public string sport { get; set; }
            public List<string> tags { get; set; }
            public string state { get; set; }
            public TeamColors teamColors { get; set; }
        }

        public class Criterion
        {
            public int id { get; set; }
            public string label { get; set; }
            public string englishLabel { get; set; }
            public List<object> order { get; set; }
            public string occurrenceType { get; set; }
            public string lifetime { get; set; }
        }

        public class BetOfferType
        {
            public int id { get; set; }
            public string name { get; set; }
            public string englishName { get; set; }
        }

        public class Outcome
        {
            public object id { get; set; }
            public string label { get; set; }
            public string englishLabel { get; set; }
            public int odds { get; set; }
            public string participant { get; set; }
            public string type { get; set; }
            public object betOfferId { get; set; }
            public DateTime changedDate { get; set; }
            public int participantId { get; set; }
            public string oddsFractional { get; set; }
            public string oddsAmerican { get; set; }
            public string status { get; set; }
            public string cashOutStatus { get; set; }
            public int? line { get; set; }
        }

        public class OddsStats
        {
            public bool unexpectedOddsTrend { get; set; }
            public long outcomeId { get; set; }
            public int startingOdds { get; set; }
            public string startingOddsFractional { get; set; }
            public string startingOddsAmerican { get; set; }
        }

        public class BetOffer
        {
            public object id { get; set; }
            public Criterion criterion { get; set; }
            public BetOfferType betOfferType { get; set; }
            public int eventId { get; set; }
            public List<Outcome> outcomes { get; set; }
            public List<string> tags { get; set; }
            public int sortOrder { get; set; }
            public string cashOutStatus { get; set; }
            public OddsStats oddsStats { get; set; }
            public DateTime? closed { get; set; }
        }

        public class MatchClock
        {
            public int minute { get; set; }
            public int second { get; set; }
            public int minutesLeftInPeriod { get; set; }
            public int secondsLeftInMinute { get; set; }
            public string period { get; set; }
            public bool running { get; set; }
        }

        public class Score
        {
            public string home { get; set; }
            public string away { get; set; }
            public string who { get; set; }
        }

        public class Home2
        {
            public int yellowCards { get; set; }
            public int redCards { get; set; }
            public int corners { get; set; }
        }

        public class Away2
        {
            public int yellowCards { get; set; }
            public int redCards { get; set; }
            public int corners { get; set; }
        }

        public class Football
        {
            public Home2 home { get; set; }
            public Away2 away { get; set; }
        }

        public class Statistics
        {
            public Football football { get; set; }
        }

        public class LiveStatistic
        {
            public string occurrenceTypeId { get; set; }
            public int count { get; set; }
        }

        public class LiveData
        {
            public int eventId { get; set; }
            public MatchClock matchClock { get; set; }
            public Score score { get; set; }
            public Statistics statistics { get; set; }
            public List<LiveStatistic> liveStatistics { get; set; }
        }

        public class Event
        {
            public Event2 @event { get; set; }
            public List<BetOffer> betOffers { get; set; }
            public LiveData liveData { get; set; }
        }

        public class Term
        {
            public string type { get; set; }
            public string id { get; set; }
            public string termKey { get; set; }
            public string localizedName { get; set; }
            public string parentId { get; set; }
            public string englishName { get; set; }
        }

        public class Category
        {
            public int id { get; set; }
            public string englishName { get; set; }
            public string localizedName { get; set; }
        }

        public class CategoryGroup
        {
            public string categoryGroupName { get; set; }
            public List<Category> categories { get; set; }
            public string localizedName { get; set; }
        }

        public class NapoleonResp
        {
            public List<Event> events { get; set; }
            public List<Term> terms { get; set; }
            public List<string> activeTermIds { get; set; }
            public string soonMode { get; set; }
            public List<CategoryGroup> categoryGroups { get; set; }
            public List<string> activeCategories { get; set; }
            public List<string> activeEventTypes { get; set; }
            public List<string> eventTypes { get; set; }
            public string defaultEventType { get; set; }
        }
    }
}
