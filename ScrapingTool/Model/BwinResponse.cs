﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScrapingTool.Model
{
    public class BwinResponse
    {
        public class Name
        {
            public string value { get; set; }
            public string sign { get; set; }
        }

        public class Name2
        {
            public string value { get; set; }
            public string sign { get; set; }
        }

        public class SourceName
        {
            public string value { get; set; }
            public string sign { get; set; }
        }

        public class Result
        {
            public int id { get; set; }
            public double odds { get; set; }
            public Name2 name { get; set; }
            public SourceName sourceName { get; set; }
            public string visibility { get; set; }
        }

        public class Detailed
        {
            public int group { get; set; }
            public int index { get; set; }
            public int? subIndex { get; set; }
        }

        public class Grouping
        {
            public string grid { get; set; }
            public List<Detailed> detailed { get; set; }
        }

        public class Game
        {
            public int id { get; set; }
            public Name name { get; set; }
            public List<Result> results { get; set; }
            public int templateId { get; set; }
            public int categoryId { get; set; }
            public string resultOrder { get; set; }
            public string combo1 { get; set; }
            public string combo2 { get; set; }
            public string visibility { get; set; }
            public string category { get; set; }
            public bool isMain { get; set; }
            public Grouping grouping { get; set; }
            public int? balanced { get; set; }
            public string attr { get; set; }
            public double? spread { get; set; }
        }

        public class Name3
        {
            public string value { get; set; }
            public string sign { get; set; }
            public string @short { get; set; }
            public string shortSign { get; set; }
        }

        public class Participant
        {
            public int participantId { get; set; }
            public Name3 name { get; set; }
        }

        public class Name4
        {
            public string value { get; set; }
            public string sign { get; set; }
        }

        public class VideoStream
        {
            public string provider { get; set; }
            public string status { get; set; }
            public string providerStreamId { get; set; }
        }

        public class Addons
        {
            public int betRadar { get; set; }
            public string betBuilderFixture { get; set; }
            public VideoStream videoStream { get; set; }
            public bool? statistics { get; set; }
        }

        public class Name5
        {
            public string value { get; set; }
            public string sign { get; set; }
        }

        public class Sport
        {
            public string type { get; set; }
            public int id { get; set; }
            public Name5 name { get; set; }
        }

        public class Name6
        {
            public string value { get; set; }
            public string sign { get; set; }
        }

        public class Competition
        {
            public int parentLeagueId { get; set; }
            public bool statistics { get; set; }
            public int sportId { get; set; }
            public string type { get; set; }
            public int id { get; set; }
            public int parentId { get; set; }
            public Name6 name { get; set; }
        }

        public class Name7
        {
            public string value { get; set; }
            public string sign { get; set; }
        }

        public class Region
        {
            public string code { get; set; }
            public int sportId { get; set; }
            public string type { get; set; }
            public int id { get; set; }
            public int parentId { get; set; }
            public Name7 name { get; set; }
        }

        public class Fixture
        {
            public List<Game> games { get; set; }
            public List<Participant> participants { get; set; }
            public string id { get; set; }
            public Name4 name { get; set; }
            public int sourceId { get; set; }
            public string source { get; set; }
            public string fixtureType { get; set; }
            public string context { get; set; }
            public Addons addons { get; set; }
            public string stage { get; set; }
            public int groupId { get; set; }
            public string liveType { get; set; }
            public bool liveAlert { get; set; }
            public DateTime startDate { get; set; }
            public DateTime cutOffDate { get; set; }
            public Sport sport { get; set; }
            public Competition competition { get; set; }
            public Region region { get; set; }
            public string viewType { get; set; }
        }

        public class BwinResp
        {
            public List<Fixture> fixtures { get; set; }
            public int totalCount { get; set; }
            public int totalSports { get; set; }
            public int totalRegions { get; set; }
            public int totalCompetitions { get; set; }
        }
    }
}
