﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Net.Http;

namespace BotProper.Classes
{
   public class  Bet777
    {
        private List<string> listNames1Bet777;
        private List<string> listNames2Bet777;
        string url = "https://www.bet777.be/tennis/";
        private List<string> listOddsBet777;
        private List<string> listOdds2Bet777;
        private List<Match> listMatchBet777;
        IWait<IWebDriver> wait;
        private int countIterationOdds;
        IWebDriver driver;
        string html;

        public Bet777()
        {
             PhantomJSDriverService service = PhantomJSDriverService.CreateDefaultService();
              service.IgnoreSslErrors = true;
              service.LoadImages = false;
              service.ProxyType = "none";
              driver = new PhantomJSDriver(service);
              driver.Navigate().GoToUrl(url);           
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30.00));
            driver.Manage().Window.Size = new Size(1024, 768);
            initList();
           
            
            
            countIterationOdds = 0;
            loadWebPageBet777();
            populateListMatchBet777();
        }

        private void initList()
        {
            listNames1Bet777 = new List<string>();
            listNames2Bet777 = new List<string>();
            listOddsBet777 = new List<string>();
            listOdds2Bet777 = new List<string>();
            listMatchBet777 = new List<Match>();
        }
        public List<Match> GetMatch()
        {
            return listMatchBet777;
        }
        public void loadWebPageBet777()
        {
            extractNomsJoueurs();
            var links2 = driver.FindElements(By.XPath("//ul/li/dl/dd/ul/li/span"));
            List<IWebElement> list = links2.ToList();
            int attempt = 0;
            while (attempt < 2)
            {
                try
                {
                    foreach (var odd in list)
                    {
                        String oddies = odd.Text;
                        oddies.Replace(" ", String.Empty);
                        if (countIterationOdds == 0 || Double.Parse(oddies)<0)
                        {
                            //on ajoute pas dans liste
                        }
                        else {
                            if (countIterationOdds % 2 == 0)
                            {
                                listOddsBet777.Add(oddies);
                            }
                            else
                            {
                                listOdds2Bet777.Add(oddies);
                            }
                            countIterationOdds++;
                        }
                    }
                }
                catch(StaleElementReferenceException e)
                {

                }
                attempt++;
            }
        }
            
        private void extractNomsJoueurs()
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("team_betting")));
            var links1 = driver.FindElements(By.ClassName("team_betting"));
            html = driver.PageSource;
            foreach (var ele in links1.ToList())
            {               
                    try
                    {
                        var elementWeb = ele.FindElements(By.TagName("span"));
                        foreach(var elm in elementWeb.ToList())
                        {
                            listNames1Bet777.Add(elm.Text);
                        }
                        break;
                    }
                    catch (StaleElementReferenceException e)
                    {
                    Console.WriteLine("big souci fréro");
                    }               
            }
              for (int i = 0; i < listNames1Bet777.Count(); i++)
              {
                  if (i % 2 == 1)
                  {
                      listNames2Bet777.Add(listNames1Bet777[i]);
                  }
              }
              foreach (string name in listNames2Bet777.ToList())
              {
                  listNames1Bet777.Remove(name);
              }
        }
        public void populateListMatchBet777()
        {
            for (int i = 0; i < listNames1Bet777.Count(); i++)
            {
                double cote1 = Double.Parse(listOddsBet777[i]);
                cote1 = cote1 / 100;
                double cote2 = Double.Parse(listOdds2Bet777[i]);
                cote2 = cote2 / 100;
                Match match = new Match(cote1, cote2, listNames1Bet777[i], listNames2Bet777[i]);//j'ai delete toString();
                listMatchBet777.Add(match);
            }
        }
        public void ElaguerJoueursBet777(Classes.Match match2, out string j1, out string j2, Boolean parenth)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            if (j1.Contains("("))
            {
                parenth = true;
            }
            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            if (parenth == true)
            {
                j1.Substring(j1.Length - 3);
                parenth = false;
            }
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            if (j2.Contains("("))
            {
                parenth = true;
            }
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
            if (parenth == true)
            {
                j2.Substring(j2.Length - 3);
                parenth = false;
            }

        }
    }
}
