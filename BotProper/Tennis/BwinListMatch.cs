﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;


namespace BotProper.Classes
{
    class BwinListMatch :InterfaceSite
    {
        private List<string> listNames1Bwin;
        private List<string> listNames2Bwin;
        HtmlWeb web;
        string url = "https://sports.bwin.be/fr/sports/5/paris-sportifs/tennis#sportId=5";
        private List<string> listOddsBwin;
        private List<string> listOdds2Bwin;
        private List<Match> listMatchBwin;
        private int countIterationOdds;

        public BwinListMatch()
        {
            listNames1Bwin = new List<string>();
            listNames2Bwin = new List<string>();
            listOdds2Bwin = new List<string>();
            listOddsBwin = new List<string>();
            listMatchBwin = new List<Match>();
            web = new HtmlWeb();
            countIterationOdds = 0;
            loadWebPageBwin();
            populateListMatchBwin();
        }

        public void loadWebPageBwin()
        {
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load(url);
            extractNomsJoueurs(doc);
            var odds = doc.DocumentNode.SelectNodes("//*[contains(@class, 'mg-option-button__option-odds')]");
            foreach (var odd in odds)
            {
                String oddies = odd.InnerText;
                oddies.Replace(" ", String.Empty);
                if (countIterationOdds % 2 == 0)
                {
                    listOddsBwin.Add(oddies);
                }
                else
                {
                    listOdds2Bwin.Add(oddies);
                }
                countIterationOdds++;
            }
        }
        private void extractNomsJoueurs(HtmlAgilityPack.HtmlDocument doc)
        {
            var links = doc.DocumentNode.SelectNodes("//*[contains(@class, 'mg-event-name-label')]");
            foreach (var link in links)
            {
                // string[] tokens = link.InnerText.Split('-');//pcq sinon nom genre jean-michel passait pas
                string[] tokens = link.InnerText.Split(new string[] { "- " }, StringSplitOptions.None);
                listNames1Bwin.Add(tokens[0]);
                listNames2Bwin.Add(tokens[1]);
            }
        }
        public void populateListMatchBwin()
        {
            for (int i = 0; i < listOddsBwin.Count(); i++)
            {
                double cote1; double cote2;
                try
                {
                    cote1 = Double.Parse(listOddsBwin[i]);
                    cote1 = cote1 / 100;
                }
                catch(Exception e)
                {
                    Console.Write(e.Message);
                    cote1 = 1.0;
                }
                try
                {
                    cote2 = Double.Parse(listOdds2Bwin[i]);
                    cote2 = cote2 / 100;
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                    cote2 = 1.0;
                }
                Match match = new Match(cote1, cote2, listNames1Bwin[i], listNames2Bwin[i]);//j'ai delete toString();
                listMatchBwin.Add(match);
            }
        }
        public List<Match> getMatch()
        {
            return this.listMatchBwin;
        }
        public String getName()
        {
            return "bwin";
        }
        public void ElaguerString(Classes.Match match2, out string j1, out string j2,Boolean parenth)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            if (j1.Contains("("))
            {
                parenth = true;
            }
            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            if (parenth == true)
            {
               j1= j1.Remove(j1.Length - 3,3);
                parenth = false;
            }
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            if (j2.Contains("("))
            {
                parenth = true;
            }
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
            if (parenth == true)
            {
                j2=j2.Remove(j2.Length - 3, 3);
                parenth = false;
            }

        }
    }
}
