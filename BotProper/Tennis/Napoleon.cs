﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotProper.Classes
{
    class Napoleon : InterfaceSite
    {
        private List<string> listNamesNap;
        private List<string> listNames2Nap;
        string url = "https://fr-be.sports.napoleongames.be/sports#filter/tennis";
        private List<string> listOddsNap;
        private List<string> listOdds2Nap;
        private List<Match> listMatchNap;
        double cote1;
        double cote2;
        int count;
        bool iter = true;
        IWait<IWebDriver> wait;
        private int countIterationOdds;
        ChromeDriver driver;
        string html;
        string htmlTmp;
        HtmlAgilityPack.HtmlDocument doc;
        public Napoleon()
        {
            var service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            var options = new ChromeOptions();
            options.AddArgument("headless");
            options.AddArgument("no-sandbox");
            options.BinaryLocation = ("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
            driver = new ChromeDriver(service, options);
            driver.Navigate().GoToUrl(url);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60.00));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("KambiBC-event-time_event-info")));
            //var BtnClick = driver.FindElementsByClassName("");
            html = driver.PageSource;
            doc = new HtmlAgilityPack.HtmlDocument();
            initList();
            doc.LoadHtml(html);
            html = driver.PageSource;
            loadPageNap();
            populateMatchNap();
            driver.Close();
            driver.Dispose();
            driver.Quit();
        }
        private void initList()
        {
            listNamesNap = new List<string>();
            listNames2Nap = new List<string>();
            listOddsNap = new List<string>();
            listOdds2Nap = new List<string>();
            listMatchNap = new List<Match>();
        }
        public void loadPageNap()
        {
            extractNoms();
            // var links2 = doc.DocumentNode.SelectNodes("//*[contains(@class, 'OutcomeOdd')]/div/span");
            var links2 = doc.DocumentNode.SelectNodes("/html/body/div/div/div/div/div/div/section/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/span");
            foreach (var odds in links2)
            {
                String oddies = odds.InnerText;
                oddies.Replace(" ", String.Empty);
                if (countIterationOdds % 2 == 0)
                {
                    listOddsNap.Add(oddies);
                }
                else
                {
                    listOdds2Nap.Add(oddies);
                }
                countIterationOdds++;
            }
        }
        public void extractNoms()
        {
            //  var noms = doc.DocumentNode.SelectNodes("//*[contains(@class, 'OutcomeName')]//span");
            var noms = doc.DocumentNode.SelectNodes("/html/body/div/div/div/div/div/div/section/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/span");
            count = 0;
            foreach (var link in noms)
            {
                listNamesNap.Add(link.InnerText);
            }
        }

        private void listNomsDiv()
        {
            for (int i = 0; i < listNamesNap.Count(); i++)
            {
                if (i % 2 != 0)
                {
                    listNames2Nap.Add(listNamesNap[i]);
                }
            }
            foreach (string name in listNames2Nap)
            {
                listNamesNap.Remove(name);
            }
        }

        public void populateMatchNap()
        {
            listNomsDiv();
            for (int i = 0; i < listNamesNap.Count(); i++)
            {
                try
                {
                    cote1 = Double.Parse(listOddsNap[i]);
                    cote2 = Double.Parse(listOdds2Nap[i]);
                    cote1 = cote1 / 100;
                    cote2 = cote2 / 100;
                    if (cote1 <= 1)
                    {
                        cote1 = cote1 * 10;
                    }
                    if (cote2 <= 1)
                    {
                        cote2 = cote2 * 10;
                    }
                    if (cote1 > 10 && cote2 > 10)
                    {
                        if (cote1 < cote2)
                        {
                            while (cote1 > 10)
                            {
                                cote1 = cote1 / 10;
                            }
                        }
                        else
                        {
                            while (cote2 > 10)
                            {
                                cote2 = cote2 / 10;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    cote1 = 1.0;
                    cote2 = 1.0;
                }
                Match match = new Match(cote1, cote2, listNamesNap[i], listNames2Nap[i]);//j'ai delete toString();
                listMatchNap.Add(match);
            }
        }
        public void ElaguerString(Classes.Match match2, out string j1, out string j2, Boolean parenth)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.Replace(".", "");
            j2 = j2.Replace(".", "");
            j1 = j1.Replace(",", "");
            j2 = j2.Replace(",", "");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            if (j1.Contains("("))
            {
                parenth = true;
            }

            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            if (parenth == true)
            {
                j1 = j1.Remove(j1.Length - 3, 3);
                parenth = false;
            }
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            if (j2.Contains("("))
            {
                parenth = true;
            }
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
            if (parenth == true)
            {
                j2 = j2.Remove(j2.Length - 3, 3);
                parenth = false;
            }

        }

        public List<Match> getMatch()
        {
            return listMatchNap;
        }
        public String getName()
        {
            return "Circus";
        }
    }
}
