﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotProper.Classes
{
    public class Match
    {
        private double cote1;
        private double cote2;
        private string Joueur1;
        private string Joueur2;

        public Match(double cote1, double cote2, string Joueur1, string Joueur2)
        {
            this.cote1 = cote1;
            this.cote2 = cote2;
            this.Joueur1 = Joueur1;
            this.Joueur2 = Joueur2;

        }
        public double getCote1()
        {
            return this.cote1;
        }
        public double getCote2()
        {
            return this.cote2;
        }
        public String getJoueur1()
        {
            return this.Joueur1;
        }
        public String getJoueur2()
        {
            return this.Joueur2;
        }
        public override string ToString()
        {
            return "Les joueurs sont: " + this.Joueur1 + " " + this.Joueur2 + "Cote 1: " + this.cote1 + "Cote2: " + this.cote2;
        }
    }
}
