﻿using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotProper.Classes
{
    class Bet777WithAgile:InterfaceSite
    {
        private List<string> listNames1Bet777;
        private List<string> listNames2Bet777;
        string url = "https://www.bet777.be/tennis/";
        private List<string> listOddsBet777;
        private List<string> listOdds2Bet777;
        private List<Match> listMatchBet777;
        double cote1;
        double cote2;
        IWait<IWebDriver> wait;
        private int countIterationOdds;
        PhantomJSDriver driver;
        string html;
        HtmlAgilityPack.HtmlDocument doc;
        public Bet777WithAgile()
        {
            PhantomJSDriverService service = PhantomJSDriverService.CreateDefaultService();
            service.IgnoreSslErrors = true;
            service.LoadImages = false;
            service.HideCommandPromptWindow = true;
            service.ProxyType = "none";
            driver = new PhantomJSDriver(service);
            driver.Navigate().GoToUrl(url);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30.00));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("team_betting")));
            html = driver.PageSource;
            driver.Close();
            driver.Dispose();
            driver.Quit();
            doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            initList();
            loadPageWeb777();
            populateListMatchBet777();
        }
        private void initList()
        {
            listNames1Bet777 = new List<string>();
            listNames2Bet777 = new List<string>();
            listOddsBet777 = new List<string>();
            listOdds2Bet777 = new List<string>();
            listMatchBet777 = new List<Match>();
        }
        public void loadPageWeb777()
        {
            extractNoms();
            var links2 = doc.DocumentNode.SelectNodes("//ul/li/dl/dd/ul/li/span");
            foreach(var odds in links2)
            {
                String oddies = odds.InnerText;
                oddies.Replace(" ", String.Empty);
                if (countIterationOdds % 2 == 0&&double.Parse(oddies)>0)
                {
                    listOddsBet777.Add(oddies);
                }
                else
                {
                    if(double.Parse(oddies) > 0)
                    {
                        listOdds2Bet777.Add(oddies);
                    }
                }
                countIterationOdds++;
            }
        }
        public void extractNoms()
        {
            var noms = doc.DocumentNode.SelectNodes("//*[contains(@class, 'team_betting')]");
            foreach (var name in noms)
            {
                string[] tokens = name.InnerText.Split(new string[] { "vs" }, StringSplitOptions.None);
                listNames1Bet777.Add(tokens[0]);
                listNames2Bet777.Add(tokens[1]);
             }
        }
        public void populateListMatchBet777()
        {
            
            for (int i = 0; i < listNames1Bet777.Count(); i++)
            {
                 cote1 =Double.Parse(listOddsBet777[i]);
                 cote2 = Double.Parse(listOdds2Bet777[i]);
                cote1 = cote1 / 100;
                cote2 = cote2 / 100;
                if (cote1 <= 1)
                {
                    listOddsBet777.RemoveAt(i);
                }
                if (cote2 <= 1)
                {
                    listOdds2Bet777.RemoveAt(i);
                }
                if (cote1 > 10 && cote2 > 10)
                {
                    if (cote1 < cote2)
                    {
                        while (cote1 > 10)
                        {
                            cote1 = cote1 / 10;
                        }
                    }
                    else
                    {
                        while (cote2 > 10)
                        {
                            cote2 = cote2 / 10;
                        }
                    }
                }
                Match match = new Match(cote1, cote2, listNames1Bet777[i], listNames2Bet777[i]);//j'ai delete toString();
                listMatchBet777.Add(match);               
            }
        }
        public List<Match> getMatch()
        {
            return listMatchBet777;
        }
        public void ElaguerString(Classes.Match match2, out string j1, out string j2, Boolean parenth)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            if (j1.Contains("("))
            {
                parenth = true;
            }

            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            if (parenth == true)
            {
                j1=j1.Remove(j1.Length - 3,3);
                parenth = false;
            }
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            if (j2.Contains("("))
            {
                parenth = true;
            }
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
            if (parenth == true)
            {
                j2=j2.Remove(j2.Length - 3,3);
                parenth = false;
            }

        }
        public String getName()
        {
            return "Bet777";
        }
    }

}

