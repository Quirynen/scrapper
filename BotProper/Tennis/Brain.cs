﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordsMatching;

namespace BotProper.Classes
{
    class Brain
    {
        private Boolean parenth = false;
        BwinListMatch bwin;
        LadbrokeslistMatch ladbrokes;
        Bet777WithAgile bet777;
        Circus circus;
        Napoleon napo;
        List<MatchFind> matchFound;
        double cote1;
        double cote2;
        string siteUn;
        string siteDeux;
        int nbMatchfound;
        public Brain()
        {
            //napo = new Napoleon();
            bwin = new BwinListMatch();
            ladbrokes = new LadbrokeslistMatch();
           // bet777 = new Bet777WithAgile();
            circus = new Circus();
            matchFound = new List<MatchFind>();
            searchOtherList();
          //  compareLadbrokes(bet777);
            compareLadbrokes(circus);
            nbMatchfound = matchFound.Count();
        }
        public void searchOtherList()
        {
             nbMatchfound = 0;
            foreach (Classes.Match match in ladbrokes.getMatch())
            {
                string joueur1, joueur2;
                ladbrokes.ElaguerString(match, out joueur1, out joueur2);
                foreach (Classes.Match match2 in bwin.getMatch())
                {
                    string michcopy, michcopy2;
                    bwin.ElaguerString(match2, out michcopy, out michcopy2, parenth);
                    nbMatchfound = checkSureBetOrdre(nbMatchfound, match, joueur1, joueur2, match2, michcopy, michcopy2);
                }
            }
        }
        public void compareLadbrokes(InterfaceSite o)
        {
            foreach (Classes.Match match in o.getMatch())
            {
                string joueur1, joueur2;
                o.ElaguerString(match, out joueur1, out joueur2, parenth);
                foreach (Classes.Match match2 in ladbrokes.getMatch())
                {
                    string j1, j2;
                    ladbrokes.ElaguerString(match2, out j1, out j2);
                    if (matchString(j1, joueur1) >= checkScore(j1, joueur1) && matchString(j2, joueur2) >= checkScore(j2, joueur2))
                    //if(checkScoreSift(j1,joueur1)<=5&&checkScoreSift(j2,joueur2)<=5)
                    {
                        var listTmp = matchFound.ToList();
                        bool isPresent = false;
                        foreach (Classes.MatchFind matchFounds in listTmp)
                        {
                            string j1a= matchFounds.getJoueur1();
                            string j2a= matchFounds.getJoueur2();
                            matchFounds.ElaguerJoueurs(matchFounds, out j1a, out j2a, false);
                            if (!(matchString(joueur1, j1a) >= checkScore(joueur1, j1a)) && !(matchString(joueur2,j2a) >= checkScore(joueur2, j2a)))
                           // if(!(checkScoreSift(joueur1,j1a)<=5) && !(checkScoreSift(joueur2,j2a)<=5))
                            {
                                isPresent = false;
                            }
                            else
                            {
                                cote1 = Math.Max(matchFounds.getCote1(), match.getCote1());
                                cote2 = Math.Max(matchFounds.getCote2(), match.getCote2());
                                matchFounds.setCote1(cote1);
                                matchFounds.setCote2(cote2);
                                if (cote1 == match.getCote1())
                                {
                                    matchFounds.setSiteUn(o.getName());
                                }
                                if (cote2 == match.getCote2())
                                {
                                    matchFounds.setSite2(o.getName());
                                }
                                isPresent = true;
                                break;
                            }
                        }
                        if (isPresent == false)
                        {
                            initCoteComp(match, match2, out cote1, out cote2);
                            getSiteBet(match.getCote1(), match.getCote2());
                            matchFound.Add(new Classes.MatchFind(cote1, cote2, match.getJoueur1(), match.getJoueur2(), siteUn, siteDeux));
                        }
                    }

                }
            }
        }
        private double matchString(string a, string b)
        {
            MatchsMaker mk = new MatchsMaker(a, b);
            return mk.Score;
        }
        private double checkScore(string a, string b)
        {
            double score = 0.89;
            if (a.Contains("/") || b.Contains("/"))
            {
                score = 0.65;
            }
            return score;
        }
        private double checkScoreSift(string a, string b)
        {
            int c = 5;
            return MatchString.Sift4.CommonDistance(a, b, c);
        }
        private int checkSureBetOrdre(int nbMatchfound, Classes.Match match, string joueur1, string joueur2, Classes.Match match2, string michcopy, string michcopy2)
        {
            // if (matchString(michcopy, joueur1) >= checkScore(michcopy, joueur1) && matchString(michcopy2, joueur2) >= checkScore(michcopy2, joueur2))
            if (checkScoreSift(michcopy,joueur1)<=5&&checkScoreSift(michcopy2,joueur2)<=5)
            {
                double cote1Match1, cote2Match1;
                initCoteComp(match, match2, out cote1Match1, out cote2Match1);
                getSite(cote1Match1, cote2Match1);
                matchFound.Add(new Classes.MatchFind(cote1, cote2, match.getJoueur1(), match.getJoueur2(), siteUn, siteDeux));
            }
            return nbMatchfound;
        }

        private void initCoteComp(Classes.Match match, Classes.Match match2, out double cote1Match1, out double cote2Match1)
        {
            cote1Match1 = match.getCote1();
            cote2Match1 = match.getCote2();
            double cote1Match2 = match2.getCote1();
            double cote2Match2 = match2.getCote2();
            cote1 = Math.Max(cote1Match1, cote1Match2);
            cote2 = Math.Max(cote2Match1, cote2Match2);
        }

        private void getSite(double cote1Match1, double cote2Match1)
        {
            if (cote1 == cote1Match1)
            {
                siteUn = "ladbrokes";
            }
            else
            {
                siteUn = "bwin";
            }
            if (cote2 == cote2Match1)
            {
                siteDeux = "ladbrokes";
            }
            else
            {
                siteDeux = "bwin";
            }
        }
       private void getSiteBet(double cote1Match1, double cote2Match1)
        {
            if (cote1 == cote1Match1)
            {
                siteUn = "Circus";
            }
            else
            {
                siteUn = "ladbrokes";
            }
            if (cote2 == cote2Match1)
            {
                siteDeux = "Circus";
            }
            else
            {
                siteDeux = "ladbrokes";
            }
        }
        public Classes.Bet777WithAgile getBet777()
        {
            return this.bet777;
        }
        public Classes.BwinListMatch getBwin()
        {
            return this.bwin;
        }
        public Classes.LadbrokeslistMatch getLad()
        {
            return this.ladbrokes;
        }
        public Classes.Circus getCircus()
        {
            return this.circus;
        }
        public List<MatchFind> getmatchFind()
        {
            return this.matchFound;
        }
        public int getNbMatchFound()
        {
            return this.nbMatchfound;
        }
    }
}
