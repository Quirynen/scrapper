﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace BotProper.Classes
{
    class LadbrokeslistMatch 
    {
        private int i;
        private List<string> listNamesLadTest;
        private List<string> listNamesLad;
        private List<string> listOddsLad ;
        private List<string> listOdds2Lad;
        private List<Match> listMatchLad;
        private int countIterationOddsLad;

        public LadbrokeslistMatch()
        {
            listNamesLad = new List<string>();
            listNamesLadTest = new List<string>();
            listOdds2Lad = new List<string>();
            listOddsLad = new List<string>();
            listMatchLad = new List<Match>();
            countIterationOddsLad = 0;
            i = 0;
            loadWebPageLadbrokes();
            populateListMatchLad();
        }
        public void loadWebPageLadbrokes()
        {
            string url = "https://sports.ladbrokes.be/fr/Tennis";
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load(url);
            extractNomsJoueursLad(doc);
            var odds = doc.DocumentNode.SelectNodes("//*[contains(@class, 'price dec')]");
            foreach (var odd in odds)
            {
                String oddies = odd.InnerText;
                oddies.Replace(" ", String.Empty);
                if (countIterationOddsLad % 2 == 0)
                {
                    listOddsLad.Add(oddies);
                }
                else
                {
                    listOdds2Lad.Add(oddies);
                }
                countIterationOddsLad++;
            }
        }
        private void extractNomsJoueursLad(HtmlAgilityPack.HtmlDocument doc)
        {
            var links = doc.DocumentNode.SelectNodes("//*[contains(@class, 'seln-name')]");
            foreach (var link in links)
            {
                listNamesLadTest.Add(link.InnerText);
            }
            for (int i = 0; i < listNamesLadTest.Count(); i++)
            {
                if (i % 2 == 0)
                {
                    listNamesLad.Add(listNamesLadTest[i]);
                }
            }
            foreach (string name in listNamesLad)
            {
                listNamesLadTest.Remove(name);
            }
        }
        private void populateListMatchLad()
        {           
                for (int i = 0; i < listNamesLadTest.Count(); i++)
                {
                try
                {
                    double cote1;
                    double cote2;
                    cote1 = Double.Parse(listOddsLad[i]);
                    cote2 = Double.Parse(listOdds2Lad[i]);
                    if (listOddsLad[i].Length > 3)
                    {
                        cote1 = cote1 / 1000;
                        if (listOdds2Lad[i].Length > 3)
                        {
                            cote2 = cote2 / 1000;
                        }
                        else
                        {
                            cote2 = cote2 / 100;
                        }
                    }
                    else
                    {
                        cote1 = Double.Parse(listOddsLad[i]);
                        cote1 = cote1 / 100;
                        cote2 = Double.Parse(listOdds2Lad[i]);
                        cote2 = cote2 / 100;
                    }
                    while (cote1 <= 1)
                    {
                        cote1 = cote1 * 10;
                    }
                    while (cote2 <= 1)
                    {
                        cote2 = cote2 * 10;
                    }
                    Match match = new Match(cote1, cote2, listNamesLad[i].ToString(), listNamesLadTest[i].ToString());
                    listMatchLad.Add(match);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);

                }
            }
          
            
        }
        public List<Match> getMatch()
        {
            return listMatchLad;
        }
  
        public void ElaguerString(Classes.Match match, out string joueur1, out string joueur2)
        {
            joueur1 = match.getJoueur1();
            joueur2 = match.getJoueur2();
            joueur1.TrimStart();
            joueur2.TrimStart();
            joueur1 = joueur1.Replace(" ", String.Empty);
            joueur2 = joueur2.Replace(" ", String.Empty);
            joueur1 = joueur1.Replace(",", String.Empty);
            joueur2 = joueur2.Replace(",", String.Empty);
            joueur1 = joueur1.ToUpper();
            joueur2 = joueur2.ToUpper();
        }
    }
}
