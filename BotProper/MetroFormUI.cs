﻿using BotProper.Classes;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BotProper
{
    public partial class SureBet : MetroForm
    {
        private Brain brain;
        public SureBet()
        {
            brain = new Brain();
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
        }

        private void SureBet_Load(object sender, EventArgs e)
        {
            int nbBwin = 0;
            foreach (Classes.Match mich in brain.getBwin().getMatch())
            {
                this.metroGrid2.Rows.Add(mich.getJoueur1(), mich.getJoueur2(), mich.getCote1(), mich.getCote2());
                nbBwin++;
            }
            foreach (Classes.Match mich2 in brain.getLad().getMatch())
            {
                this.metroGrid3.Rows.Add(mich2.getJoueur1(), mich2.getJoueur2(), mich2.getCote1(), mich2.getCote2());
            }
         /*   foreach (Classes.Match matchbet777 in brain.getBet777().getMatch())
            {
                this.metroGrid1.Rows.Add(matchbet777.getJoueur1(), matchbet777.getJoueur2(), matchbet777.getCote1(), matchbet777.getCote2());
            }*/
            foreach (Classes.Match matchcircus in brain.getCircus().getMatch())
            {
                string j1= matchcircus.getJoueur1(); string j2= matchcircus.getJoueur2();
                brain.getCircus().ElaguerString(matchcircus, out j1, out j2 ,false);
                this.metroGrid6.Rows.Add(j1, j2, matchcircus.getCote1(), matchcircus.getCote2());
            }
            foreach (Classes.MatchFind mf in brain.getmatchFind())
            {
                metroGrid4.Rows.Add(mf.getJoueur1(), mf.getJoueur2(), mf.getCote1(), mf.getSite1(), mf.getCote2(), mf.getSite2());
                checkSurebet(mf);
            }
            metroTextBox1.Text = (nbBwin.ToString());
            metroTextBox4.Text = brain.getNbMatchFound().ToString();
            metroTextBox2.Text = brain.getLad().getMatch().Count().ToString();
           // metroTextBox3.Text = brain.getBet777().getMatch().Count().ToString();
            metroTextBox6.Text = brain.getCircus().getMatch().Count().ToString();
        }
        private void checkSurebet(Classes.MatchFind mf)
        {
            if ((1 / mf.getCote1() + (1 / mf.getCote2())) < 1)
            {
                metroTextBox5.Text = "Des surebets ont été trouvés.";
                metroTextBox5.ForeColor = Color.Red;
                metroGrid5.Rows.Add(mf.getJoueur1(), mf.getJoueur2(), mf.getCote1(),mf.getSite1(),mf.getCote2(),mf.getSite2());

            }
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }
    } 
}
