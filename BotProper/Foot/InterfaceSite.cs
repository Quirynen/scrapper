﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotProper.Foot
{

        public interface InterfaceSite
        {
            List< MatchFoot> getMatch();
            void ElaguerString(MatchFoot match, out string joueur1, out string joueur2);
            String getName();
           Dictionary<int,MatchFoot> getDico();
        }
}
