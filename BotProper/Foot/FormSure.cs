﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BotProper.Foot
{
    public partial class FormSure : Form
    {
        private Brain brain;
        public FormSure(Brain brain)
        {
            this.brain = brain;
            InitializeComponent();
        }

        private void FormSure_Load(object sender, EventArgs e)
        {
            foreach (var match in brain.GetSurebt())
            {
                double maxCote = Math.Max(match.getCote1(), Math.Max(match.getCote2(),match.getCote3()));
                double total = ((1 / match.getCote1()) + (1 / match.getCote2()) + (1 / match.getCote3()));
                double gain= ((1 / maxCote) /total )*100;
                Decimal gain2 = Decimal.Parse(gain.ToString());
                string gain3 = gain2.ToString()+" %";
             //   string gain = ((1-((1 / match.getCote1()) + (1 / match.getCote2()) + (1 / match.getCote3())))*100).ToString()+" %";
                dtSure.Rows.Add(match.getJoueur1(), match.getJoueur2(), match.getCote1(), match.getSite1(), match.getCote2(), match.getSite2(), match.getCote3(), match.getSite3(),gain3);
            }
        }

        private void FormSure_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
