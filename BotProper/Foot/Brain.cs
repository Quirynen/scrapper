﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordsMatching;

namespace BotProper.Foot
{
    public class Brain
    {
        BwinFoot bwin;
        LadFoot ladbrokes;
        Bet777Foot bet777;
        CircusFoot circus;
        Dictionary<int, MatchFind> dicoMf;
        double cote1;
        double cote2;
        double cote3;
        string siteUn;
        string siteDeux;
        string siteTrois;
        int i ;
        int nbMatchfound=0;
        List<MatchFind> matchFound;
        List<MatchFind> surebet;

        public Brain()
        {
            bwin = new BwinFoot();
            ladbrokes = new LadFoot();
            circus = new CircusFoot();
         //   bet777 = new Bet777Foot();
            matchFound = new List<MatchFind>();
            i = 0;
            surebet = new List<Foot.MatchFind>();
            dicoMf = new Dictionary<int, MatchFind>();
            ParcoursDico();
            ParcoursCompareLadbrokesDico(circus);
           // ParcoursCompareLadbrokesDico(bet777);
            ElaguerFind();
            parcoursMf();
        }
        public BwinFoot GetBwin()
        {
            return this.bwin;
        }
        public LadFoot getLad()
        {
            return this.ladbrokes;
        }
        public CircusFoot getCircus()
        {
            return this.circus;
        }
        public Bet777Foot getBet777()
        {
            return this.bet777;
        }
        private void ParcoursDico()
        {
            nbMatchfound = 0;
            foreach (var match in ladbrokes.getDico().Values)
            {
                string joueur1, joueur2;
                ladbrokes.ElaguerString(match, out joueur1, out joueur2);
                foreach (MatchFoot match2 in bwin.getDico().Values)
                {
                    string player1, player2;
                    bwin.ElaguerString(match2, out player1, out player2);
                    checkSureBetOrdre(nbMatchfound, match, joueur1, joueur2, match2, player1, player2);
                }
            }
        }
        private void ParcoursCompareLadbrokesDico(InterfaceSite o)
        {
            foreach (MatchFoot match in o.getDico().Values)
            {
                string joueur1, joueur2;
                o.ElaguerString(match, out joueur1, out joueur2);
                foreach (MatchFoot match2 in bwin.getDico().Values)
                {
                    string j1, j2;
                    bwin.ElaguerString(match2, out j1, out j2);
                    ActionsIfPresent(o, match, joueur1, joueur2, match2, j1, j2);
                }
            }
        }
        private double matchString(string a, string b)
        {
            MatchsMaker mk = new MatchsMaker(a, b);
            return mk.Score;
        }
        public void parcoursMf()
        {
            foreach (var mf in this.matchFound)
            {
                checkSurebet(mf);
            }
        }

        public List<Foot.MatchFind> GetSurebt()
        {
            return this.surebet;
        }
        private void checkSurebet(Foot.MatchFind mf)
        {
            if (((1 / mf.getCote1() + (1 / mf.getCote2())) + (1 / mf.getCote3())) < 1)
            {
                surebet.Add(mf);
            }
        }
        public void compareLadbrokes(InterfaceSite o)
        {
            foreach (MatchFoot match in o.getMatch())
            {
                string joueur1, joueur2;
                o.ElaguerString(match, out joueur1, out joueur2);
                foreach (MatchFoot match2 in ladbrokes.getMatch())
                {
                    string j1, j2;
                    ladbrokes.ElaguerString(match2, out j1, out j2);
                    ActionsIfPresent(o, match, joueur1, joueur2, match2, j1, j2);
                }
            }
        }

        private void ActionsIfPresent(InterfaceSite o, MatchFoot match, string joueur1, string joueur2, MatchFoot match2, string j1, string j2)
        {
            if ((matchString(j1, joueur1) >= 0.65) && (matchString(j2, joueur2) >= 0.65))
            {
                bool isPresent = false;
                foreach (MatchFind matchFounds in dicoMf.Values)
                {
                    string j1a = matchFounds.getJoueur1();
                    string j2a = matchFounds.getJoueur2();
                    matchFounds.ElaguerString(matchFounds, out j1a, out j2a);
                    if (!(matchString(joueur1, j1a) >= 0.65) && !((matchString(joueur2, j2a) >= 0.65)))
                    {
                        isPresent = false;
                    }
                    else if((matchString(joueur1, j1a) >= 0.65) && ((matchString(joueur2, j2a) >= 0.65)))
                    {
                        cote1 = Math.Max(matchFounds.getCote1(), match.getCote1());
                        cote2 = Math.Max(matchFounds.getCote2(), match.getCote2());
                        cote3 = Math.Max(matchFounds.getCote3(), match.getCote3());
                        matchFounds.setCote1(cote1);
                        matchFounds.setCote2(cote2);
                        matchFounds.setCote3(cote3);
                        if (cote1 == match.getCote1())
                        {
                            matchFounds.setSiteUn(o.getName());
                        }
                        if (cote2 == match.getCote2())
                        {
                            matchFounds.setSite2(o.getName());
                        }
                        if (cote3 == match.getCote3())
                        {
                            matchFounds.setSite3(o.getName());
                        }
                        isPresent = true;
                        break;
                    }
                }
                if (isPresent == false)
                {
                    initCoteComp(match, match2, out cote1, out cote2, out cote3);
                    getSiteBet(match.getCote1(), match.getCote2(), match.getCote3(), o);
                    MatchFind m = new MatchFind(cote1, cote2, cote3, match.getJoueur1(), match.getJoueur2(), siteUn, siteDeux, siteTrois);
                    dicoMf.Add(i,m);
                    i++;
                    matchFound.Add(m);
                }
            }
        }

        private void getSiteBet(double cote1Match1, double cote2Match1,double cote3Match1,InterfaceSite o)
        {
            if (cote1 == cote1Match1)
            {
                siteUn = o.getName(); ;
            }
            else
            {
                siteUn = "ladbrokes";
            }
            if (cote2 == cote2Match1)
            {
                siteDeux = o.getName();
            }
            else
            {
                siteDeux = "ladbrokes";
            }
            if (cote3 == cote3Match1)
            {
                siteTrois = o.getName();
            }
            else
            {
                siteDeux = "ladbrokes";
            }
        }
        public void searchOtherList()
        {
            nbMatchfound = 0;
            foreach (MatchFoot match in ladbrokes.getMatch())
            {
                string joueur1, joueur2;
                ladbrokes.ElaguerString(match, out joueur1, out joueur2);
                foreach (MatchFoot match2 in bwin.getMatch())
                {
                    string player1, player2;
                    bwin.ElaguerString(match2, out player1, out player2);
                    checkSureBetOrdre(nbMatchfound, match, joueur1, joueur2, match2, player1, player2);
                }
            }
        }
        private void checkSureBetOrdre(int nbMatchfound, MatchFoot match, string joueur1, string joueur2, MatchFoot match2, string player1, string player2)
        {
            if ((matchString(player1, joueur1) >=0.60) && (matchString(player2, joueur2) >= 0.60))
            {
                double cote1Match1, cote2Match1,cote3Match1;
                initCoteComp(match, match2, out cote1Match1, out cote2Match1,out cote3Match1);
                getSiteBL(cote1Match1, cote2Match1,cote3Match1);
                MatchFind m= new Foot.MatchFind(cote1, cote2, cote3, match.getJoueur1(), match.getJoueur2(), siteUn, siteDeux, siteTrois);
                dicoMf.Add(i, m);
                i++;
                matchFound.Add(m);                
            }
        }
        private void ElaguerFind()
        {
            for(int i = 0; i< matchFound.Count-1; i++)
            {
                if((matchFound[i].getJoueur1()==matchFound[i+1].getJoueur1())&& (matchFound[i].getJoueur2() == matchFound[i + 1].getJoueur2()))
                {
                    matchFound.RemoveAt(i + 1);
                }
            }
        }
        private void getSiteBL(double cote1Match1, double cote2Match1,double cote3Match1)
        {
            if (cote1 == cote1Match1)
            {
                siteUn = "ladbrokes";
            }
            else
            {
                siteUn = "bwin";
            }
            if (cote2 == cote2Match1)
            {
                siteDeux = "ladbrokes";
            }
            else
            {
                siteDeux = "bwin";
            }
            if (cote3 == cote3Match1)
            {
                siteTrois = "ladbrokes";
            }
            else
            {
                siteTrois = "bwin";
            }
        }
        private void initCoteComp(MatchFoot match, MatchFoot match2, out double cote1Match1, out double cote2Match1,out double cote3Match1)
        {
            cote1Match1 = match.getCote1();
            cote2Match1 = match.getCote2();
            cote3Match1 = match.getCote3();
            double cote1Match2 = match2.getCote1();
            double cote2Match2 = match2.getCote2();
            double cote3Match2 = match2.getCote3();
            cote1 = Math.Max(cote1Match1, cote1Match2);
            cote2 = Math.Max(cote2Match1, cote2Match2);
            cote3 = Math.Max(cote3Match1,cote3Match2);
        }
        public List<MatchFind> getmatchFind()
        {
            return this.matchFound;
        }

    }
}
