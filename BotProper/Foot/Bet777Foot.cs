﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.PhantomJS;

namespace BotProper.Foot
{
    public class Bet777Foot:InterfaceSite
    {
        private List<string> listNames1Bet777;
        private List<string> listNames2Bet777;
        HtmlWeb web;
        string url = "https://www.bet777.be/football/";
        private List<string> listOddsBet777;
        private List<string> listOdds2Bet777;
        private List<string> listOdds3Bet777;
        private List<MatchFoot> listMatchBet777;
        private int countIterationOdds;
        private int iterationNames;
        ChromeDriver driver;
        IWait<OpenQA.Selenium.IWebDriver> wait;
        string html;
        Dictionary<int, MatchFoot> dicoBet;

        public Bet777Foot()
        {
            dicoBet = new Dictionary<int, MatchFoot>();
             var service = ChromeDriverService.CreateDefaultService();
              service.HideCommandPromptWindow = true;
              var options = new ChromeOptions();
              options.AddArgument("headless");
              options.AddArgument("no-sandbox");
              driver = new ChromeDriver(service, options);
            driver.Navigate().GoToUrl(url);
            waitForElements();
            html = driver.PageSource;
            driver.Quit();
            driver.Dispose();
            initList();
            web = new HtmlWeb();
            countIterationOdds = 0;
            iterationNames = 0;
            loadPageBet777();
            populateListMatchBwin();
        }
        public Dictionary<int,MatchFoot> getDico()
        {
            return this.dicoBet;
        }
        private void initList()
        {
            listNames1Bet777 = new List<string>();
            listNames2Bet777 = new List<string>();
            listOddsBet777 = new List<string>();
            listOdds2Bet777 = new List<string>();
            listOdds3Bet777 = new List<string>();
            listMatchBet777 = new List<MatchFoot>();
        }

        private void waitForElements()
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60.00));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("//*[contains(@class, 'event-details-row-team')]")));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("bet-odds")));
        }

        public void loadPageBet777()
        {
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            extractNomsJoueurs(doc);
            extractOdds(doc);
        }
        private void parcoursOdds(HtmlNodeCollection nodeColl)
        {
            foreach (var odd in nodeColl)
            {
                switch (countIterationOdds)
                {
                    case 0:
                        listOddsBet777.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 1:
                        listOdds2Bet777.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 2:
                        listOdds3Bet777.Add(odd.InnerText);
                        countIterationOdds = 0;
                        break;
                }
            }
        }
        private void extractOdds(HtmlDocument doc)
        {
            var odds = doc.DocumentNode.SelectNodes("//*[contains(@class, 'events-container-league')]//*[contains(@class,'bet-odds-number')]");
            parcoursOdds(odds);
        }
        private void parcoursNames(HtmlNodeCollection names)
        {
            foreach (var link in names)
            {
                if (link.InnerText != "X")
                {
                    if (iterationNames % 2 == 0)
                    {
                        listNames1Bet777.Add(link.InnerText);
                    }
                    else
                    {
                        listNames2Bet777.Add(link.InnerText);
                    }
                    iterationNames++;
                }
            }
        }
        private void extractNomsJoueurs(HtmlAgilityPack.HtmlDocument doc)
        {
            var links = doc.DocumentNode.SelectNodes("//*[contains(@class, 'events-container-league')]//*[contains(@class, 'event-details-row-team')]//*[contains(@class,'event-details-side-left')]");
            parcoursNames(links);
        }
        public String getName()
        {
            return "bet777";
        }
        public void populateListMatchBwin()
        {
            for (int i = 0; i < listOddsBet777.Count(); i++)
            {
                double cote1; double cote2; double cote3;
                cote1 = Double.Parse(listOddsBet777[i]);
                cote1 = cote1 / 100;
                cote2 = Double.Parse(listOdds2Bet777[i]);
                cote2 = cote2 / 100;
                cote3 = Double.Parse(listOdds3Bet777[i]);
                cote3 = cote3 / 100;
                MatchFoot match = new MatchFoot(cote1, cote2, cote3, listNames1Bet777[i], listNames2Bet777[i]);//j'ai delete toString();
                dicoBet.Add(i, match);
                listMatchBet777.Add(match);
            }
        }
        public List<MatchFoot> getMatch()
        {
            return this.listMatchBet777;
        }
        public void ElaguerString(MatchFoot match2, out string j1, out string j2)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
        }
    }
}
