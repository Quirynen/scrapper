﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.PhantomJS;

namespace BotProper.Foot
{
    public class CircusFoot: InterfaceSite
    {
        private List<string> listNames1Circus;
        private List<string> listNames2Circus;
        HtmlWeb web;
        string url = "https://www.circus.be/fr/sport/paris-sportifs/844";
        private List<string> listOddsCircus;
        private List<string> listOdds2Circus;
        private List<string> listOdds3Circus;
        private List<MatchFoot> listMatchCircus;
        private int countIterationOdds;
        private int iterationNames;
        ChromeDriver driver;
        IWait<IWebDriver> wait;
        string html;
        string htmlTmp;
        HtmlAgilityPack.HtmlDocument doc;
        Dictionary<int, MatchFoot> dicoCircus;

        public CircusFoot()
        {
            Navigation();
            waitForElements();
            initList();
            dicoCircus = new Dictionary<int, MatchFoot>();
            HtmlWeb web = new HtmlWeb();
            doc = new HtmlDocument();
            web = new HtmlWeb();
            countIterationOdds = 0;
            iterationNames = 0;
            checkOtherPages();
            driver.Quit();
            driver.Dispose();
            populateMatchCircus();
        }

        private void Navigation()
        {
              var service = ChromeDriverService.CreateDefaultService();
              service.HideCommandPromptWindow = true;
              var options = new ChromeOptions();
              options.AddArgument("headless");
              options.AddArgument("no-sandbox");
              driver = new ChromeDriver(service, options);
            driver.Navigate().GoToUrl(url);
        }
        private void checkOtherPages()
        {
           htmlTmp = html;
           do {
               //doc.LoadHtml(html);
               ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", driver.FindElementByCssSelector(".fa.fa-chevron-right"));
               driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
               wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("outcome-item")));
               htmlTmp = html;
               html = driver.PageSource;
           }while (!html.Equals(htmlTmp)) ;
            loadPageCircus();
        }
        private void initList()
        {
            listNames1Circus = new List<string>();
            listNames2Circus = new List<string>();
            listOddsCircus = new List<string>();
            listOdds2Circus = new List<string>();
            listOdds3Circus = new List<string>();
            listMatchCircus = new List<MatchFoot>();
        }

        private void waitForElements()
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60.00));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.CssSelector(".fa.fa-chevron-right")));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("outcome-item")));
        }
        public void loadPageCircus()
        {
 
            doc.LoadHtml(html);
            extractNomsJoueurs(doc);
            extractOdds(doc);
        }
        private void extractOdds(HtmlDocument doc)
        {
            var odds = doc.DocumentNode.SelectNodes("//*[contains(@class, 'league__container')]//*[contains(@class,'OutcomeOdd')]//span");
            parcoursOdds(odds);
        }
        private void parcoursOdds(HtmlNodeCollection nodeColl)
        {
            foreach (var odd in nodeColl)
            {
                switch (countIterationOdds)
                {
                    case 0:
                        listOddsCircus.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 1:
                        listOdds2Circus.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 2:
                        listOdds3Circus.Add(odd.InnerText);
                        countIterationOdds = 0;
                        break;
                }
            }
        }
        private void extractNomsJoueurs(HtmlAgilityPack.HtmlDocument doc)
        {
            var links = doc.DocumentNode.SelectNodes("//*[contains(@class, 'league__container')]//*[contains(@class,'OutcomeName')]//span");
            parcoursNames(links);
        }

        private void parcoursNames(HtmlNodeCollection names)
        {
            foreach (var link in names)
            {
                if (link.InnerText != "Match nul"&&link.InnerText!="X")
                {
                    if (iterationNames % 2 == 0)
                    {
                        listNames1Circus.Add(link.InnerText);
                    }
                    else
                    {
                        listNames2Circus.Add(link.InnerText);
                    }
                    iterationNames++;
                }
            }
        }

        public void populateMatchCircus()
        {
            for (int i = 0; i < listNames1Circus.Count(); i++)
            {
                double cote1; double cote2; double cote3;
                try
                {
                    cote1 = Double.Parse(listOddsCircus[i]);
                    cote1 = cote1 / 100;
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                    cote1 = 1.0;
                }
                try
                {
                    cote2 = Double.Parse(listOdds2Circus[i]);
                    cote2 = cote2 / 100;
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                    cote2 = 1.0;
                }
                try
                {
                    cote3 = Double.Parse(listOdds3Circus[i]);
                    cote3 = cote3 / 100;
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                    cote3 = 1.0;
                }
                cote1=multCote(cote1);
                cote2=multCote(cote2);
                cote3=multCote(cote3);
                MatchFoot match = new MatchFoot(cote1, cote2, cote3, listNames1Circus[i], listNames2Circus[i]);//j'ai delete toString();
                dicoCircus.Add(i, match);
                listMatchCircus.Add(match);
            }
        }
        public Dictionary<int,MatchFoot> getDico()
        {
            return this.dicoCircus;
        }
        private double multCote(double a)
        {
            while (a < 1)
            {
                a = a * 10;
            }
            return a;
        }
        public List<MatchFoot> getMatch()
        {
            return this.listMatchCircus;
        }
        public String getName()
        {
            return "circus";
        }
        public void ElaguerString(MatchFoot match2, out string j1, out string j2)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
        }
    }
}
