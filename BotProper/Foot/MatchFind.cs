﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotProper.Foot
{
    public class MatchFind
    {
        private double cote1;
        private double cote2;
        private double cote3;
        private string Joueur1;
        private string Joueur2;
        private string site1;
        private string site2;
        private string site3;

        public MatchFind(double cote1, double cote2,double cote3, string Joueur1, string Joueur2, string site1, string site2,string site3)
        {
            this.cote1 = cote1;
            this.cote2 = cote2;
            this.cote3 = cote3;
            this.Joueur1 = Joueur1;
            this.Joueur2 = Joueur2;
            this.site1 = site1;
            this.site2 = site2;
            this.site3 = site3;

        }
        public double getCote1()
        {
            return this.cote1;
        }
        public double getCote2()
        {
            return this.cote2;
        }
        public double getCote3()
        {
            return this.cote3;
        }
        public String getJoueur1()
        {
            return this.Joueur1;
        }
        public String getJoueur2()
        {
            return this.Joueur2;
        }
        public String getSite1()
        {
            return this.site1;
        }
        public String getSite2()
        {
            return this.site2;
        }
        public String getSite3()
        {
            return this.site3;
        }
        public void setCote1(double a)
        {
            this.cote1 = a;
        }
        public void setCote2(double a)
        {
            this.cote2 = a;
        }
        public void setCote3(double a)
        {
            this.cote3 = a;
        }
        public void setSiteUn(string a)
        {
            this.site1 = a;
        }
        public void setSite2(string a)
        {
            this.site2 = a;
        }
        public void setSite3(string a)
        {
            this.site3 = a;
        }
        public override string ToString()
        {
            return "Les joueurs sont: " + this.Joueur1 + " " + this.Joueur2 + "Cote 1: " + this.cote1 + "Cote2: " + this.cote2+ "Cote3: "+this.cote3;
        }
        public void ElaguerString(MatchFind match2, out string j1, out string j2)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            j1 = j1.Replace("/", String.Empty);
            j2 = j2.Replace("/", String.Empty);
            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
        }
    }
}
