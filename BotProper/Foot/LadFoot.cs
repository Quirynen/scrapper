﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace BotProper.Foot
{
    public class LadFoot :InterfaceSite
    {
        private List<string> listNames2;
        private List<string> listNames1;
        private List<string> listOddsLad;
        private List<string> listOdds2Lad;
        private List<string> listOdds3Lad;
        private List<MatchFoot> listMatchLad;
        private int countIterationOdds;
        private int countIterationNames;
        private Dictionary<int,MatchFoot>dicoLad;

        public LadFoot()
        {
            initList();
            countIterationOdds = 0;
            countIterationNames = 0;
            loadWebPageLadbrokes();
            populateListMatchLad();
        }

        private void initList()
        {
            dicoLad = new Dictionary<int, MatchFoot>();
            listNames1 = new List<string>();
            listNames2 = new List<string>();
            listOddsLad = new List<string>();
            listOdds2Lad = new List<string>();
            listOdds3Lad = new List<string>();
            listMatchLad = new List<MatchFoot>();
        }

        public void loadWebPageLadbrokes()
        {
            string url = "https://sports.ladbrokes.be/fr/Football";
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load(url);
            extractNomsJoueursLad(doc);
            extractOdds(doc);
        }

        private void extractOdds(HtmlAgilityPack.HtmlDocument doc)
        {
            var odds = doc.DocumentNode.SelectNodes("*//div[@id='main-area']//*[contains(@class, 'seln')]//*[contains(@class, 'price dec')]");
            foreach (var odd in odds)
            {
                switch (countIterationOdds)
                {
                    case 0:
                        listOddsLad.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 1:
                        listOdds2Lad.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 2:
                        listOdds3Lad.Add(odd.InnerText);
                        countIterationOdds = 0;
                        break;
                }
            }
        }

        private void extractNomsJoueursLad(HtmlAgilityPack.HtmlDocument doc)
        {
            var links = doc.DocumentNode.SelectNodes("*//div[@id='main-area']//*[contains(@class, 'seln')]//*[contains(@class, 'seln-name')]");
            foreach(var name in links)
            {
                if (!(name.InnerText == "Egalité")) {
                    if (countIterationNames % 2 == 0)
                    {
                        listNames1.Add(name.InnerText);
                    }
                    else
                    {
                        listNames2.Add(name.InnerText);
                    }
                    countIterationNames++;
                }
                
            }
        }
        public String getName()
        {
            return "ladbrokes";
        }
        private void populateListMatchLad()
        {
            for (int i = 0; i < listNames1.Count(); i++)
            {
                try
                {
                    double cote1;
                    double cote2;
                    double cote3;
                    cote1 = Double.Parse(listOddsLad[i]);
                    cote2 = Double.Parse(listOdds2Lad[i]);
                    cote3 = Double.Parse(listOdds3Lad[i]);
                    if (listOddsLad[i].Length > 3)
                    {
                        cote1 = cote1 / 1000;
                        if (listOdds2Lad[i].Length > 3)
                        {
                            cote2 = cote2 / 1000;
                        }
                        else
                        {
                            cote2 = cote2 / 100;
                        }
                        if (listOdds3Lad[i].Length > 3)
                        {
                            cote3 = cote3 / 1000;
                        }
                        else
                        {
                            cote3 = cote3 / 100;
                        }
                    }
                    else
                    {
                        cote1 = Double.Parse(listOddsLad[i]);
                        cote1 = cote1 / 100;
                        cote2 = Double.Parse(listOdds2Lad[i]);
                        cote2 = cote2 / 100;
                        cote3 = Double.Parse(listOdds3Lad[i]);
                        cote3 = cote3 / 100;
                    }
                    while (cote1 <= 1)
                    {
                        cote1 = cote1 * 10;
                    }
                    while (cote2 <= 1)
                    {
                        cote2 = cote2 * 10;
                    }
                    while (cote3 <= 1)
                    {
                        cote3 = cote3 * 10;
                    }
                    MatchFoot match = new MatchFoot(cote1, cote2,cote3, listNames1[i], listNames2[i]);
                    dicoLad.Add(i, match);
                    listMatchLad.Add(match);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }


        }
        public Dictionary<int, MatchFoot> getDico()
        {
            return this.dicoLad;
        }
        public List<MatchFoot> getMatch()
        {
            return listMatchLad;
        }

        public void ElaguerString(MatchFoot match, out string joueur1, out string joueur2)
        {
            joueur1 = match.getJoueur1();
            joueur2 = match.getJoueur2();
            joueur1.TrimStart();
            joueur2.TrimStart();
            joueur1 = joueur1.Replace(" ", String.Empty);
            joueur2 = joueur2.Replace(" ", String.Empty);
            joueur1 = joueur1.Replace(",", String.Empty);
            joueur2 = joueur2.Replace(",", String.Empty);
            joueur1 = joueur1.ToUpper();
            joueur2 = joueur2.ToUpper();
        }
    }
}
