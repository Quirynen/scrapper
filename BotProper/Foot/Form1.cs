﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordsMatching;

namespace BotProper
{
    public partial class Form1 : Form
    {

        private Foot.BwinFoot bwin;
        private Foot.LadFoot ladbrokes;
        private Foot.Bet777Foot bet777;
        private Foot.CircusFoot circus;
        private Foot.Brain brain;

        public Form1()
        {
            brain = new Foot.Brain();
            WindowState = FormWindowState.Maximized;
            InitializeComponent();
            this.BringToFront();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            foreach(var match in brain.GetBwin().getMatch())
            {
                dtBwin.Rows.Add(match.getJoueur1(), match.getJoueur2(), match.getCote1(), match.getCote2(), match.getCote3());
            }
            foreach (var match in brain.getLad().getMatch())
            {
                dtLad.Rows.Add(match.getJoueur1(), match.getJoueur2(), match.getCote1(), match.getCote2(), match.getCote3());
            }
            foreach (var match in brain.getCircus().getMatch())
            {
                dtCircus.Rows.Add(match.getJoueur1(), match.getJoueur2(), match.getCote1(), match.getCote2(), match.getCote3());
            }
            foreach (var match in brain.getmatchFind())
            {
                dtFound.Rows.Add(match.getJoueur1(), match.getCote1(), match.getSite1(), match.getCote2(), match.getSite2(), match.getCote3(), match.getJoueur2(), match.getSite3());               
            }
            foreach (var match in brain.getBet777().getMatch())
            {
                dtBet777.Rows.Add(match.getJoueur1(), match.getJoueur2(), match.getCote1(), match.getCote2(), match.getCote3());
            }
            surebet.Hide();
            Ouvrir.Hide();
            if (brain.GetSurebt().Count > 0)
            {
                surebet.Show();
                Ouvrir.Show();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void button2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Restart();
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Ouvrir_Click(object sender, EventArgs e)
        {
            Foot.FormSure ft = new Foot.FormSure(brain);
            ft.Show();
        }
    }
}
