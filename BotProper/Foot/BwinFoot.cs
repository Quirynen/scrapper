﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BotProper.Foot
{
   public class BwinFoot:InterfaceSite
    {
        private List<string> listNames1Bwin;
        private List<string> listNames2Bwin;
        HtmlWeb web;
        string url = "https://sports.bwin.be/fr/sports/5/paris-sportifs/football#sportId=4";
        private List<string> listOddsBwin;
        private List<string> listOdds2Bwin;
        private List<string> listOdds3Bwin;
        private List<MatchFoot> listMatchBwin;
        private int countIterationOdds;
        private int iterationNames;
         ChromeDriver driver;
        IWait<IWebDriver> wait;
        string html;
        Dictionary<int, MatchFoot> dicoBwin;

        public BwinFoot()
        {
                var service = ChromeDriverService.CreateDefaultService();
                service.HideCommandPromptWindow = true;
                var options = new ChromeOptions();
                options.AddArgument("headless");
                options.AddArgument("no-sandbox");
                driver = new ChromeDriver(service, options);
            driver.Navigate().GoToUrl(url);
            waitForElements();
            html = driver.PageSource;
            driver.Quit();
            driver.Dispose();
            initList();
            web = new HtmlWeb();
            countIterationOdds = 0;
            iterationNames = 0;
            loadWebPageBwin();
            dicoBwin = new Dictionary<int, MatchFoot>();
            populateListMatchBwin();
        }

        private void initList()
        {
            listNames1Bwin = new List<string>();
            listNames2Bwin = new List<string>();
            listOddsBwin = new List<string>();
            listOdds2Bwin = new List<string>();
            listOdds3Bwin = new List<string>();
            listMatchBwin = new List<MatchFoot>();
        }

        private void waitForElements()
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60.00));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("mg-result-column3")));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("mg-option-button__option-odds")));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("mb-option-button__option-name")));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("mb-option-button__option-odds")));
        }

        public void loadWebPageBwin()
        {
            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            extractNomsJoueurs(doc);
            extractOdds(doc);
        }
        public Dictionary<int, MatchFoot> getDico()
        {
            return this.dicoBwin;
        }
        private void parcoursOdds(HtmlNodeCollection nodeColl)
        {
            foreach (var odd in nodeColl)
            {
                switch (countIterationOdds)
                {
                    case 0:
                        listOddsBwin.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 1:
                        listOdds2Bwin.Add(odd.InnerText);
                        countIterationOdds++;
                        break;
                    case 2:
                        listOdds3Bwin.Add(odd.InnerText);
                        countIterationOdds = 0;
                        break;
                }
            }
        }
        private void extractOdds(HtmlDocument doc)
        {
            var odds = doc.DocumentNode.SelectNodes("//div[contains(@class, 'mg-result-column3')]//*[contains(@class,'mg-option-button__option-odds')]");
            parcoursOdds(odds);
            var odds2 = doc.DocumentNode.SelectNodes("//*[contains(@class, 'mb-option-button__option-odds')]");
            parcoursOdds(odds2);
        }
        private void parcoursNames(HtmlNodeCollection names)
        {
            foreach (var link in names)
            {
                if (link.InnerText != "X")
                {
                    if (iterationNames % 2 == 0)
                    {
                        listNames1Bwin.Add(link.InnerText);
                    }
                    else
                    {
                        listNames2Bwin.Add(link.InnerText);
                    }
                    iterationNames++;
                }
            }
        }
        private void extractNomsJoueurs(HtmlAgilityPack.HtmlDocument doc)
        {
            var links = doc.DocumentNode.SelectNodes("//div[contains(@class, 'mg-event-name-label')]//span");
            parcoursNames(links);
            var links2 = doc.DocumentNode.SelectNodes("//div[contains(@class, 'mb-option-button__option-name')]");
            parcoursNames(links2);
        }
        public void populateListMatchBwin()
        {
            for (int i = 0; i < listNames1Bwin.Count(); i++)
            {
                double cote1; double cote2;double cote3;
                cote1 = Double.Parse(listOddsBwin[i]);
                cote1 = cote1 / 100;
                cote2 = Double.Parse(listOdds2Bwin[i]);
                cote2 = cote2 / 100;
                cote3 = Double.Parse(listOdds3Bwin[i]);
                cote3 = cote3 / 100;   
                MatchFoot match = new MatchFoot(cote1, cote2,cote3, listNames1Bwin[i], listNames2Bwin[i]);//j'ai delete toString();
                dicoBwin.Add(i, match);
                listMatchBwin.Add(match);
            }
        }
        public List<MatchFoot> getMatch()
        {
            return this.listMatchBwin;
        }
        public String getName()
        {
            return "bwin";
        }
        public void ElaguerString(MatchFoot match2, out string j1, out string j2)
        {
            j1 = match2.getJoueur1();
            j2 = match2.getJoueur2();
            j1 = j1.Replace("ë", "e");
            j2 = j2.Replace("ë", "e");
            j1 = j1.ToUpper();
            j2 = j2.ToUpper();
            j1 = j1.Replace(" ", String.Empty);
            j1 = j1.Replace("-", String.Empty);
            j1 = j1.Replace("(", String.Empty);
            j1 = j1.Replace(")", String.Empty);
            j1 = j1.Replace("\r", String.Empty);
            j1 = j1.Replace("\n", String.Empty);
            j2 = j2.Replace(" ", String.Empty);
            j2 = j2.Replace("-", String.Empty);
            j2 = j2.Replace("(", String.Empty);
            j2 = j2.Replace(")", String.Empty);
            j2 = j2.Replace("\r", String.Empty);
            j2 = j2.Replace("\n", String.Empty);
        }
    }
}

