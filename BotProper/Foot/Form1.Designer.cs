﻿namespace BotProper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.matchBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.matchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtBwin = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cote3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.dtLad = new System.Windows.Forms.DataGridView();
            this.Team1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Team2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vic1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nul = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vic2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtFound = new System.Windows.Forms.DataGridView();
            this.j1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.site1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.site2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.j2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.site3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.dtBet777 = new System.Windows.Forms.DataGridView();
            this.t1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.t2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.odd1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.odd2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.odd3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surebet = new System.Windows.Forms.Label();
            this.Ouvrir = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dtCircus = new System.Windows.Forms.DataGridView();
            this.Eq1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Eq2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Co1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Co2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Co3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.matchBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBwin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBet777)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCircus)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Match Bwin";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.button2.Location = new System.Drawing.Point(1804, 943);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 33);
            this.button2.TabIndex = 12;
            this.button2.Text = "Restart";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dtBwin
            // 
            this.dtBwin.BackgroundColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtBwin.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtBwin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtBwin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.Cote3});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtBwin.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtBwin.Location = new System.Drawing.Point(24, 32);
            this.dtBwin.Name = "dtBwin";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtBwin.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtBwin.RowTemplate.Height = 24;
            this.dtBwin.Size = new System.Drawing.Size(616, 266);
            this.dtBwin.TabIndex = 28;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Equipe1";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 75;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Equipe2";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 75;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Cote1";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Cote2";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 75;
            // 
            // Cote3
            // 
            this.Cote3.HeaderText = "Cote3";
            this.Cote3.Name = "Cote3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(30, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Match Ladbrokes";
            // 
            // dtLad
            // 
            this.dtLad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtLad.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Team1,
            this.Team2,
            this.Vic1,
            this.Nul,
            this.Vic2});
            this.dtLad.Location = new System.Drawing.Point(24, 337);
            this.dtLad.Name = "dtLad";
            this.dtLad.RowTemplate.Height = 24;
            this.dtLad.Size = new System.Drawing.Size(616, 273);
            this.dtLad.TabIndex = 30;
            // 
            // Team1
            // 
            this.Team1.HeaderText = "Team1";
            this.Team1.Name = "Team1";
            // 
            // Team2
            // 
            this.Team2.HeaderText = "Team2";
            this.Team2.Name = "Team2";
            // 
            // Vic1
            // 
            this.Vic1.HeaderText = "Vic1";
            this.Vic1.Name = "Vic1";
            // 
            // Nul
            // 
            this.Nul.HeaderText = "Nul";
            this.Nul.Name = "Nul";
            // 
            // Vic2
            // 
            this.Vic2.HeaderText = "Vic2";
            this.Vic2.Name = "Vic2";
            // 
            // dtFound
            // 
            this.dtFound.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtFound.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.j1,
            this.c1,
            this.site1,
            this.c2,
            this.site2,
            this.c3,
            this.j2,
            this.site3});
            this.dtFound.Location = new System.Drawing.Point(748, 32);
            this.dtFound.Name = "dtFound";
            this.dtFound.RowTemplate.Height = 24;
            this.dtFound.Size = new System.Drawing.Size(905, 302);
            this.dtFound.TabIndex = 31;
            // 
            // j1
            // 
            this.j1.HeaderText = "j1";
            this.j1.Name = "j1";
            // 
            // c1
            // 
            this.c1.HeaderText = "c1";
            this.c1.Name = "c1";
            // 
            // site1
            // 
            this.site1.HeaderText = "site1";
            this.site1.Name = "site1";
            // 
            // c2
            // 
            this.c2.HeaderText = "c2";
            this.c2.Name = "c2";
            // 
            // site2
            // 
            this.site2.HeaderText = "site2";
            this.site2.Name = "site2";
            // 
            // c3
            // 
            this.c3.HeaderText = "c3";
            this.c3.Name = "c3";
            // 
            // j2
            // 
            this.j2.HeaderText = "j2";
            this.j2.Name = "j2";
            // 
            // site3
            // 
            this.site3.HeaderText = "site3";
            this.site3.Name = "site3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label3.Location = new System.Drawing.Point(24, 626);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 32;
            this.label3.Text = "Match Bet777";
            // 
            // dtBet777
            // 
            this.dtBet777.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtBet777.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.t1,
            this.t2,
            this.odd1,
            this.odd2,
            this.odd3});
            this.dtBet777.Location = new System.Drawing.Point(24, 647);
            this.dtBet777.Name = "dtBet777";
            this.dtBet777.RowTemplate.Height = 24;
            this.dtBet777.Size = new System.Drawing.Size(616, 249);
            this.dtBet777.TabIndex = 33;
            // 
            // t1
            // 
            this.t1.HeaderText = "t1";
            this.t1.Name = "t1";
            // 
            // t2
            // 
            this.t2.HeaderText = "t2";
            this.t2.Name = "t2";
            // 
            // odd1
            // 
            this.odd1.HeaderText = "odd1";
            this.odd1.Name = "odd1";
            // 
            // odd2
            // 
            this.odd2.HeaderText = "odd2";
            this.odd2.Name = "odd2";
            // 
            // odd3
            // 
            this.odd3.HeaderText = "odd3";
            this.odd3.Name = "odd3";
            // 
            // surebet
            // 
            this.surebet.AutoSize = true;
            this.surebet.ForeColor = System.Drawing.Color.Red;
            this.surebet.Location = new System.Drawing.Point(1677, 32);
            this.surebet.Name = "surebet";
            this.surebet.Size = new System.Drawing.Size(206, 17);
            this.surebet.TabIndex = 34;
            this.surebet.Text = "Des surbets sont présents wlh :";
            // 
            // Ouvrir
            // 
            this.Ouvrir.BackColor = System.Drawing.Color.Red;
            this.Ouvrir.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ouvrir.Location = new System.Drawing.Point(1680, 64);
            this.Ouvrir.Name = "Ouvrir";
            this.Ouvrir.Size = new System.Drawing.Size(139, 29);
            this.Ouvrir.TabIndex = 35;
            this.Ouvrir.Text = "Ouvrir";
            this.Ouvrir.UseVisualStyleBackColor = false;
            this.Ouvrir.Click += new System.EventHandler(this.Ouvrir_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label4.Location = new System.Drawing.Point(674, 626);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 17);
            this.label4.TabIndex = 36;
            this.label4.Text = "Circus Matchs";
            // 
            // dtCircus
            // 
            this.dtCircus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtCircus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Eq1,
            this.Eq2,
            this.Co1,
            this.Co2,
            this.Co3});
            this.dtCircus.Location = new System.Drawing.Point(677, 647);
            this.dtCircus.Name = "dtCircus";
            this.dtCircus.RowTemplate.Height = 24;
            this.dtCircus.Size = new System.Drawing.Size(645, 249);
            this.dtCircus.TabIndex = 37;
            // 
            // Eq1
            // 
            this.Eq1.HeaderText = "Eq1";
            this.Eq1.Name = "Eq1";
            // 
            // Eq2
            // 
            this.Eq2.HeaderText = "Eq2";
            this.Eq2.Name = "Eq2";
            // 
            // Co1
            // 
            this.Co1.HeaderText = "Co1";
            this.Co1.Name = "Co1";
            // 
            // Co2
            // 
            this.Co2.HeaderText = "Co2";
            this.Co2.Name = "Co2";
            // 
            // Co3
            // 
            this.Co3.HeaderText = "Co3";
            this.Co3.Name = "Co3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label5.Location = new System.Drawing.Point(751, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 17);
            this.label5.TabIndex = 38;
            this.label5.Text = "Matchs Found";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 988);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtCircus);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Ouvrir);
            this.Controls.Add(this.surebet);
            this.Controls.Add(this.dtBet777);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtFound);
            this.Controls.Add(this.dtLad);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtBwin);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.matchBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBwin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtBet777)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCircus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource matchBindingSource;
        private System.Windows.Forms.BindingSource matchBindingSource1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dtBwin;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cote3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dtLad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Team1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Team2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vic1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nul;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vic2;
        private System.Windows.Forms.DataGridView dtFound;
        private System.Windows.Forms.DataGridViewTextBoxColumn j1;
        private System.Windows.Forms.DataGridViewTextBoxColumn c1;
        private System.Windows.Forms.DataGridViewTextBoxColumn site1;
        private System.Windows.Forms.DataGridViewTextBoxColumn c2;
        private System.Windows.Forms.DataGridViewTextBoxColumn site2;
        private System.Windows.Forms.DataGridViewTextBoxColumn c3;
        private System.Windows.Forms.DataGridViewTextBoxColumn j2;
        private System.Windows.Forms.DataGridViewTextBoxColumn site3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dtBet777;
        private System.Windows.Forms.DataGridViewTextBoxColumn t1;
        private System.Windows.Forms.DataGridViewTextBoxColumn t2;
        private System.Windows.Forms.DataGridViewTextBoxColumn odd1;
        private System.Windows.Forms.DataGridViewTextBoxColumn odd2;
        private System.Windows.Forms.DataGridViewTextBoxColumn odd3;
        private System.Windows.Forms.Label surebet;
        private System.Windows.Forms.Button Ouvrir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dtCircus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Eq1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Eq2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Co1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Co2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Co3;
        private System.Windows.Forms.Label label5;
    }
}

