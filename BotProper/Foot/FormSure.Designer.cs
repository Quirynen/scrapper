﻿namespace BotProper.Foot
{
    partial class FormSure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtSure = new System.Windows.Forms.DataGridView();
            this.Equipe1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Equipe2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cote1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Site1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cote2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Site2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cote3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Site3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gains = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtSure)).BeginInit();
            this.SuspendLayout();
            // 
            // dtSure
            // 
            this.dtSure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtSure.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Equipe1,
            this.Equipe2,
            this.Cote1,
            this.Site1,
            this.Cote2,
            this.Site2,
            this.Cote3,
            this.Site3,
            this.Gains});
            this.dtSure.Location = new System.Drawing.Point(120, 120);
            this.dtSure.Name = "dtSure";
            this.dtSure.RowTemplate.Height = 24;
            this.dtSure.Size = new System.Drawing.Size(1399, 460);
            this.dtSure.TabIndex = 1;
            // 
            // Equipe1
            // 
            this.Equipe1.HeaderText = "Equipe1";
            this.Equipe1.Name = "Equipe1";
            this.Equipe1.Width = 200;
            // 
            // Equipe2
            // 
            this.Equipe2.HeaderText = "Equipe2";
            this.Equipe2.Name = "Equipe2";
            this.Equipe2.Width = 200;
            // 
            // Cote1
            // 
            this.Cote1.HeaderText = "Cote1";
            this.Cote1.Name = "Cote1";
            // 
            // Site1
            // 
            this.Site1.HeaderText = "Site1";
            this.Site1.Name = "Site1";
            // 
            // Cote2
            // 
            this.Cote2.HeaderText = "Cote2";
            this.Cote2.Name = "Cote2";
            // 
            // Site2
            // 
            this.Site2.HeaderText = "Site2";
            this.Site2.Name = "Site2";
            // 
            // Cote3
            // 
            this.Cote3.HeaderText = "Cote3";
            this.Cote3.Name = "Cote3";
            // 
            // Site3
            // 
            this.Site3.HeaderText = "Site3";
            this.Site3.Name = "Site3";
            // 
            // Gains
            // 
            this.Gains.HeaderText = "Gains";
            this.Gains.Name = "Gains";
            this.Gains.Width = 150;
            // 
            // FormSure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1531, 805);
            this.Controls.Add(this.dtSure);
            this.Name = "FormSure";
            this.Text = "FormSure";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormSure_FormClosed);
            this.Load += new System.EventHandler(this.FormSure_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtSure)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dtSure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Equipe1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Equipe2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cote1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Site1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cote2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Site2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cote3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Site3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gains;
    }
}