﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotProper.Foot
{
    public class MatchFoot
    {
        private double cote1;
        private double cote2;
        private double cote3;
        private string Equipe1;
        private string Equipe2;

        public MatchFoot(double cote1, double cote2, double cote3,string Joueur1, string Joueur2)
        {
            this.cote1 = cote1;
            this.cote2 = cote2;
            this.cote3 = cote3;
            this.Equipe1 = Joueur1;
            this.Equipe2 = Joueur2;
        }
        public double getCote1()
        {
            return this.cote1;
        }
        public double getCote2()
        {
            return this.cote2;
        }
        public double getCote3()
        {
            return this.cote3;
        }
        public String getJoueur1()
        {
            return this.Equipe1;
        }
        public String getJoueur2()
        {
            return this.Equipe2;
        }
        public override string ToString()
        {
            return "Les equipes sont: " + this.Equipe1 + " " + this.Equipe2 + "Cote 1: " + this.cote1 + "Cote2: " + this.cote2+" Cote3: "+this.cote3;
        }
    }
}
